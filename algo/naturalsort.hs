import Data.List

isSorted :: Ord a => [a] -> Bool
isSorted xs = xs == sort xs

sortedSublist :: Ord a => [a] -> ([a], [a])
sortedSublist xs = sublist' 0 xs 
  where
  sublist' :: Ord a => Int -> [a] -> ([a],[a])
  sublist' n xs 
    | n == length xs = (xs, [])
    | isSorted (take n xs) = sublist' (n+1) xs
    | otherwise = splitAt (n-1) xs

allSortedSublists :: Ord a => [a] -> [[a]]
allSortedSublists [] = []
allSortedSublists xs = sorted : allSortedSublists rest
  where
  (sorted, rest) = sortedSublist xs

merge :: (Ord a) => [a] -> [a] -> [a]
merge [] ys = ys
merge xs [] = xs
merge (x:xs) (y:ys)
  | x <= y = x:merge xs (y:ys)
  | y < x  = y:merge (x:xs) ys

bottomUpMergeSort :: Ord a => [a] -> [a]
bottomUpMergeSort xs = bUMerge' (allSortedSublists xs)
  where
  bUMerge' :: Ord a => [[a]] -> [a]
  bUMerge' [] = []
  bUMerge' (xs:xxs) = merge xs (bUMerge' xxs)
