import sys

maximum = int(sys.argv[1])
b = maximum - 1
a = b - 1

def ask(what, val):
  print "is the " + what + " less than " + str(val) + " (y=yes/n=no/a=accept num)? ",
  return raw_input()

u = maximum
l = 0
while b < maximum:
  resp = ask("denominator", b)
  if resp == "y":
    u = b
    b = int ((b + l) / 2)
  elif resp == "n":
    l = b
    b = int ((b + u) / 2)
  else:
    break

a = b - 1
u = b
l = 0
while a < b:
  resp = ask("numerator", a)

  if resp == "y":
    u = a
    a = int ((a + l) / 2)
  elif resp == "n":
    l = a
    a = int ((a + u) / 2)
  else:
    break

print "your fraction is %d/%d" % (a, b)

