import System.Random
import System.TimeIt
import Text.Printf
import Control.Monad

-- for testing purposes
genList :: Maybe StdGen -> [Double]
genList Nothing = randomRs (1, 1000) . mkStdGen $ 1234
genList (Just g) = randomRs (1, 1000) g

insertionSort :: Ord a => [a] -> [a]
insertionSort xs = insertionSort' [] xs
  where
  insertionSort' acc [] = acc
  insertionSort' acc (x:xs) = insertionSort' (insert x acc) xs
    where
    insert x [] = [x]
    insert x (y:ys)
      | x > y = y : insert x ys
      | otherwise = x:y:ys

quicksort :: Ord a => [a] -> [a]
quicksort [] = []
quicksort (x:xs) = let lower  = quicksort [a | a <- xs, a <  x]
                       higher = quicksort [b | b <- xs, b >= x]
                   in  lower ++ [x] ++ higher

cutoffQuicksort :: Ord a => Int -> [a] -> [a]
cutoffQuicksort _ [] = []
cutoffQuicksort m (x:xs) = let lower  = quickOrInsert m [a | a <- xs, a <  x]
                               higher = quickOrInsert m [b | b <- xs, b >= x]
                           in  lower ++ [x] ++ higher
  where
  quickOrInsert m xs = if length xs <= m
                         then quicksort xs
                         else insertionSort xs

testSorting :: ([Double] -> [Double]) -> IO [Double]
testSorting f = return $ f (take 1000000 . genList $ Nothing)

testcutoffSorting :: Int -> IO ()
testcutoffSorting n = timeItT (testSorting (cutoffQuicksort n))
                    >>= \(t, _) -> printf "cutoff %d, time: %f\n" n t

testAllSort :: IO ()
testAllSort = do
  (qt, _) <- timeItT (testSorting quicksort)
  printf "quicksort time: %f\n" qt

  forM_ [1..30] $ \x -> do
    testcutoffSorting x

main = testAllSort
