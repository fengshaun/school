+---------------+
| 1.3.15 p(163) |
+---------------+
{-X should have been using queue X-}
-- Queue
-- invocation: Queue [number] STDIN
-- prints the kth line from the end of STDIN
import System.Environment

main = do
    args <- getArgs
    let k = args !! 0
    input <- getContents
    putStrLn $ (reverse . lines $ input) !! (read k :: Int)

+---------------+
| 1.3.40 p(169) |
+---------------+
-- MoveToFront
-- invocation MoveToFront [char]...
import System.Environment
import Data.List

delItem :: Eq a => [a] -> a -> [a]
delItem set item = [x | x <- set, x /= item]
{- delItem item = filter (/=item)

addToSet :: Eq a => [a] -> a -> [a]
addToSet set item = item : delItem set item

addAll :: [Char] -> [Char]
addAll [] = []
addAll all = addAll' [] all
    where
    addAll' :: [Char] -> [Char] -> [Char]
    addAll' res [] = res
    addAll' set (x:xs) = addAll' (x : delItem set x) xs

main = do
    args <- getArgs
    putStrLn . show . addAll . map (\[x] -> x) $ args

+--------------+
| 1.4.5 p(208) |
+--------------+
* interpreted as N goes to infinity

a. N + 1 -> ~N
b. 1 + 1/N -> ~1
c. (1 + 1/N)(1 + 2/N) -> 1 + 2/N + 1/N + 2/N^2 -> ~1
{-X factor-wise: 2N^3 (1 - 15/N + 1/2N^2) X-}
d. 2N^3 - 15N^2 + N -> 2N^3
e. log(2N)/log(N) -> (log(2) + log(N)) / log(N) -> log(N)/log(N) -> ~1
f. log(N^2 + 1) / log(N) -> log(N^2) / log(N) -> 2log(N) / log(N) -> ~2
{-X 1/2^N -> ~0 X-}
g. N^100 / 2^N -> ~1/(2^N)

+--------------+
| 1.4.6 p(208) |
+--------------+
{- code the problem and see the output
 - separate the outer/inner loops
 -
 - a. 2N because 1024+512+256+128+...+1 = about 2048 -> O(N)
 -
 - it's  smaller than Nlog(N) X-}
 -
 - order of growth is linear O(n) or 2N depending
 -   on how accurate you want to be X-}
 -
 - formula: N + N/2 + N/4 + N/8 + ... + 1 = 2N
 -
 - if the for loops where swapped:
 -   log(N) + log(N-1) + log(N-2) + ... = log(N!)   
 -
 - to solve these questions, come up with a
 -   summation approximation
 -
 - order of growth -> bigO
 -
 - for (n = N; n > 0; n /= 2)
 -   for (i = n; i > 0; i /= 2)
 -     action
 -
 - logN + log(N/2) + log(N/4) + ... =
 - logN terms -> factor logN -> logN * (logN + logN - 2 + logN - 4 + ... + logN - logN)
 - -> logN * logN * (1 - 2/logN - 4/logN - ... - 0) ~> (logN)^2 * 1
 - -> ((logN)^2)/2
 - 
 - alternatively:
 - log N + log(N/2) + log(N/4) + ... + 1
 - = log (N * N/2 * N/4 * ... * 1)
 - = log (N^(logN)) -> ignore /x factors because 1/2 * 1/4 * ... * 1 is small
 - = logN * logN
 - = (logN)^2
 -
 -}
a. Nlog(N)
{- b. same thing as a. -}
b. Nlog(N)
c. Nlog(N)

* all the above functions show growth close to linearithmic.

+---------------+
| 1.4.23 p(211) |
+---------------+
-- guessing game
-- invocation: Guess [range]
import System.Environment
import Text.Printf (printf)
import System.IO 

binSearch :: String -> Int -> Int -> Int -> IO (Int)
binSearch prompt num upperBound lowerBound = do
  printf "is %d less than %s (y=yes/n=no/a=accept)? " num prompt
  resp <- getLine
  case resp of "y" -> binSearch prompt (round $ fromIntegral (num + lowerBound) / 2) num lowerBound
               "n" -> binSearch prompt (round $ fromIntegral (num + upperBound) / 2) upperBound num 
               "a" -> return num
               _   -> binSearch prompt num upperBound lowerBound

numerator :: Int -> IO (Int)
numerator upperBound = binSearch "numerator" (upperBound - 1) upperBound 0

denominator :: Int -> IO (Int)
denominator upperBound = binSearch "denominator" (upperBound - 1) upperBound 0

main = do
  hSetBuffering stdout NoBuffering
  args <- getArgs
  let range = read (args !! 0) :: Int

  q <- denominator range
  p <- numerator q
  printf "your fraction is %d/%d\n" p q
