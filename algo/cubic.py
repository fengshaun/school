# solve for *one* solution: ax^3 + bx^2 + cx + d = 0 for any a b c d
# error can be up to 10^-3
#
# essentially a binary search!

(a, b, c, d) = (0, 0, 0, 0)


def f(x):
    return a * (x ** 3) + b * (x ** 2) + c * x + d

def solve_cubic(error):
    l = -1.0
    r = +1.0

    while f(l) >= 0:
        l *= 2

    while f(r) <= 0:
        r *= 2

    while r - l > error:
        m = (l + r) / 2.0

        if f(m) > 0:
            r = m
        else:
            l = m

    return l

if __name__ == "__main__":
    a = int(raw_input("a "))
    b = int(raw_input("b "))
    c = int(raw_input("c "))
    d = int(raw_input("d "))

    print("solution: %.2f" % solve_cubic(0.001))
