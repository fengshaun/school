mystery :: Int -> Int -> Int
mystery a b
  | b < a = 0
  | otherwise = 1 + mystery (a - b) b

