fac :: (Integral a) => a -> a
fac 1 = 1
fac n = n * fac (n - 1)

shiftString :: String -> String
shiftString [] = []
shiftString xs = [last xs] ++ init xs

isCircularShift :: String -> String -> Bool
isCircularShift xs ys = checkShift xs ys 0
  where
  checkShift :: String -> String -> Int -> Bool
  checkShift xs ys n | n > length xs = False
                     | xs == ys = True
                     | otherwise = checkShift (shiftString xs) ys (n + 1)
