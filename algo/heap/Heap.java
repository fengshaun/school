// heap sort implementation
// using a boss array
// 0 is invalid 

public class Heap {
    private int[] data;
    private int top = 1;

    public Heap() {
        data = new int[100000];
    }

    public Heap(int size) {
        data = new int[size];
    }

    public int remove() {
        int temp = data[1];
        data[1] = data[top - 1];
        top--;

        int index = 1;
        while (index < top) {
            if (leftChild(index) >= top) {
                break;
            } else if (rightChild(index) >= top) {
                if (data[index] > data[leftChild(index)]) {
                    swap(index, leftChild(index));
                    index = leftChild(index);
                } else {
                    break;
                }
            } else if (data[leftChild(index)] < data[rightChild(index)]) {
                if (data[index] > data[leftChild(index)]) {
                    swap(index, leftChild(index));
                    index = leftChild(index);
                } else {
                    break;
                }
            } else {
                if (data[index] > data[rightChild(index)]) {
                    swap(index, rightChild(index));
                    index = rightChild(index);
                } else {
                    break;
                }
            } 
        }

        return temp;
    }

    public void insert(int targ) {
        data[top++] = targ;
        int index = top - 1;

        while (data[index] < data[parent(index)]) {
            swap(index, parent(index));
            index = parent(index);
        }
    }

    private int parent(int i) { return i / 2; } 
    private int leftChild(int i) { return i * 2; }
    private int rightChild(int i) { return i * 2 + 1; }

    private void swap(int i, int j) {
        int temp = data[i];
        data[i] = data[j];
        data[j] = temp;
    }

    public String toString() {
        String s = "";
        for (int i = 1; i < top; i++) {
            s += data[i] + " ";
        }
        return s;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public int size() {
        return top - 1;
    }
}
