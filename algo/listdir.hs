{- list the contents of a directory recursively
 - and print the sorted contents
 -}

import System.Directory
import System.Environment
import System.Posix.Files (isDirectory, getFileStatus)

main = getArgs >>= (\[path] -> getDirectoryContents path) >>= (putStrLn . show)
