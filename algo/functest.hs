import Data.Ratio
import Control.Applicative
import Control.Monad

func1 :: String -> Int
func1 _ = 4
func2 :: Int -> [Int]
func2 _ = [1,2,3]
func3 :: [Int] -> IO (Ratio Int)
func3 _ = return (2 % 3)

main = do
  {-
   - func4 = (func3 . func2 . func1) :: String -> IO (Ratio Int)
   - fmap func4 IO(String) :: IO (func4 String) :: IO (IO (Ratio Int))
   - join :: Monad m => m (m a) -> m a
   - join: removes one level of monadic structure
   -}
  answer <- (func3 . func2 . func1) <$> getLine
  answer2 <- answer
  putStrLn $ show answer2
