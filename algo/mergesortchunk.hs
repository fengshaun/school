-- sublinear extra space
import Data.List (delete)
import Data.List.Split (chunksOf)

selectionSort :: (Ord a) => [a] -> [a]
selectionSort [] = []
selectionSort [x] = [x]
selectionSort xs = y : selectionSort (delete y xs)
  where
  y = minimum xs

merge :: (Ord a) => [a] -> [a] -> [a]
merge [] ys = ys
merge xs [] = xs
merge (x:xs) (y:ys)
  | x <= y = x:merge xs (y:ys)
  | y < x  = y:merge (x:xs) ys

mergesortChunk :: (Ord a) => [[a]] -> [a]
mergesortChunk [[]] = []
mergesortChunk xxs = foldl (\acc xs -> merge acc (selectionSort xs)) [] xxs

sublinearMergeSort :: (Ord a) => Int -> [a] -> [a]
sublinearMergeSort chunk [] = []
sublinearMergeSort chunk xs = mergesortChunk . chunksOf chunk $ xs
