pop :: [a] -> (Maybe a, [a])
pop [] = (Nothing, [])
pop xs = (Just . last $ xs, init xs)

push :: a -> [a] -> [a]
push x xs = xs ++ [x]
