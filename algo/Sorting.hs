{- all the sorting algorithms -}

module Sorting where

import Data.List (splitAt, delete, sort)
import Data.List.Split (chunksOf)
import System.Random
import System.Environment
import Control.Applicative

import qualified Data.Map as M

{- utility functions -}
genList :: (RandomGen a) => Maybe a -> [Int] -- {{{
genList Nothing = randomRs (1, 1000) . mkStdGen $ 1234
genList (Just g) = randomRs (1, 1000) g
--}}}

isSorted :: (Ord a) => [a] -> Bool -- {{{
isSorted xs = xs == sort xs
-- }}}

{- merge sort
 -
 - f(n) = 2f(n/2) + 2n
 - 2f(n/2) -> dividing the list
 - 2n -> merging & copying
 -
 - f(64) = 2f(32) + 128
 - f(32) = 2f(16) + 64
 - f(16) = 2f(08) + 32
 - f(08) = 2f(04) + 16
 - f(04) = 2f(02) + 8
 -
 - all dividing operations are logN
 - each merging operation is N
 - each copying operation for merge is N
 - each merging total is 2N
 - total is 2NlogN
 - O(NlogN)
 -}
merge :: (Ord a) => [a] -> [a] -> [a]
merge [] ys = ys
merge xs [] = xs
merge (x:xs) (y:ys)
  | x <= y = x:merge xs (y:ys)
  | y < x  = y:merge (x:xs) ys

mergeSort :: (Ord a) => [a] -> [a]
mergeSort [] = []
mergeSort [x] = [x]
mergeSort xs = merge (mergeSort $ fst s) (mergeSort $ snd s)
  where
  s = splitAt (round . (/2) . fromIntegral . length $ xs) xs

quickSort :: (Ord a) => [a] -> [a]
quickSort [] = []
quickSort [x] = [x]
quickSort (x:xs) = quickSort smaller ++ [x] ++ quickSort bigger
  where
  smaller = filter (<x) xs
  bigger  = filter (>x) xs

selectionSort :: (Ord a) => [a] -> [a]
selectionSort [] = []
selectionSort [x] = [x]
selectionSort xs = y : selectionSort (delete y xs)
  where
  y = minimum xs

insertionSort :: (Ord a) => [a] -> [a]
insertionSort xs = insertionSort' [] xs
  where
  insertionSort' acc [] = acc
  insertionSort' acc (x:xs) = insertionSort' (insert x acc) xs
    where
    insert x [] = [x]
    insert x (y:ys) | x > y = y : insert x ys
                    | otherwise = x:y:ys

shellSort :: (Ord a) => [a] -> [a]
shellSort [] = []
shellSort xs = shell' 1 xs
  where
  shell' :: (Ord a) => Int -> [a] -> [a]
  shell' _ [] = []
  shell' h xs
    | h >= length xs = xs
    | otherwise = shell' (h*3 + 1) $ hSort h xs

{- h-sort
 - for an array of n items:
 -   a[0], a[h], a[2*h], ...
 -   a[1], a[h+1], a[2*h + 1], ...
 - (distance is h)
 - are sorted.
 -
 - example:
 -   15, 20, 8, 41, 14, 5, 23, 9, 2
 -
 -   1-sort: regular sort
 -   2-sort:
 -     _15_, *20*, _8_, *41*, _14_, *5*, _23_, *9*, _2_
 -     every other element is sorted
 -
 -     why? if you divide the array by 2, you are spending 1/4
 -          time to sort it
 -
 - depends on the fact that the array is partly sorted
 -}
hSort :: (Ord a) => Int -> [a] -> [a]
hSort n xs = joinHSorted . map insertionSort . columnizeEvery n $ xs

joinHSorted :: [[a]] -> [a]
joinHSorted xxs 
  | all null xxs = []
  | any null xxs = joinHSorted . filter (not . null) $ xxs
  | otherwise = let heads = map head xxs
                    tails = map tail xxs
                in  heads ++ joinHSorted tails

columnizeEvery :: Int -> [a] -> [[a]]
columnizeEvery n xs = columnize $ chunksOf n xs

columnize :: [[a]] -> [[a]]
columnize xxs 
  | all null xxs = []
  | any null xxs = columnize . filter (not . null) $ xxs
  | otherwise = let heads = map head xxs
                    tails = map tail xxs
                in  heads:columnize tails

weakSort :: Eq a => [a] -> [a]
weakSort [] = []
weakSort (x:xs) = (x : filter (==x) xs) ++ weakSort (filter (/=x) xs)

main = do
  s <- getArgs >>= (\[arg] -> case arg of 
                                "mergesort"     -> return mergeSort
                                "quicksort"     -> return quickSort
                                "selectionsort" -> return selectionSort
                                "insertionsort" -> return insertionSort
                                "shellsort"     -> return shellSort
                                "weaksort"      -> return weakSort)

  l1 <- take 20 . genList . Just <$> getStdGen
  let l2 = s l1
  putStrLn $ "before: " ++ show l1
  putStrLn $ "after:  " ++ show l2
  putStrLn $ "it " ++ (case (isSorted l2) of True  -> "is"
                                             False -> "is NOT") ++ " sorted."
