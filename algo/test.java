public class test {
  public static void main (String[] args) {
    System.out.println(mystery(6,2));
    System.out.println(mystery(30,7));
    System.out.println(mystery(5,17));
    System.out.println(mystery(2,91));
  }

  public static int mystery(int a, int b) {
    if (b < a) {
      return 0;
    }
    return (1 + mystery(a - b, b));
  }
}
