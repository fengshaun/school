{- Different searching algorithms -}

module Searching where

import System.Random
import Control.Applicative
import Data.Char (ord, chr, toUpper)
import Data.List.Split (chunksOf)

import Sorting (quickSort)

-- utility functions {{{
genList :: (RandomGen a) => Maybe a -> [Int]
genList Nothing = randomRs (1, 1000) . mkStdGen $ 1234
genList (Just g) = randomRs (1, 1000) g

contractList :: [Int] -> [(Int, Int)]
contractList [] = []
contractList l@(x:xs) = let amount = length $ filter (== x) l
                        in  (x, amount) : contractList xs
-- }}}

-- finding complement numbers {{{
findComplement :: [Int] -> Int -> (Int, Int, Bool)
findComplement [] _ = (0, 0, False)
findComplement xs target
  | s == target = (low, high, True)
  | s <  target = findComplement (tail xs) target
  | s >  target = findComplement (init xs) target
    where
    low = head xs
    high = last xs
    s = low + high

testFindComplement :: IO ()
testFindComplement = do
  let l1 = [0,1,4,67,89,99]
  let target = 100
  let (l, h, res) = findComplement [0,1,4,67,89,99] 100
  putStrLn $ "list: " ++ show l1
  putStrLn $ "target: " ++ show target
  putStrLn (case res of True  -> "found: " ++ show l ++ " " ++ show h
                        False -> "not found")
-- }}}

-- same birthday problem {{{
birthday :: [Int] -> Bool
birthday xs = not . null . filter (> 1) . map snd . contractList $ xs

testBirthday :: IO ()
testBirthday = do
  l1 <- take 60 . randomRs (1, 366) <$> getStdGen
  putStrLn $ "birthdays: " ++ show l1
  putStr   "60 people, 366 days.  Same birthday for at least 2? "
  putStrLn . show . birthday $ l1


-- }}}

-- Hashing problem
hashIndex :: String -> Int
hashIndex [] = 0
hashIndex s = sum $ map (subtract (ord 'A') . ord . toUpper) . head . chunksOf 3 $ s

main = testBirthday
