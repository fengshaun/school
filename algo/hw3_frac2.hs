{- guessing game
 - more accurate but the generation of fractions is more costly
 - assuming 1/2 is equal to 2/4 is equal to 3/6 etc.
 -}

module Main where

import System.Environment
import System.IO 
import Text.Printf (printf)
import Data.Ratio 
import Data.List 
import Control.Applicative
import Control.Monad

{- generateFractions has a growth of N^2
 - and is exhaustive for accuracy reasons.
 - a linear algorithm would be faster,
 - but less accurate
 -}
generateFractions :: Int -> [Ratio Int]
generateFractions r = (0:) . sort . nub $ [a%b | b <- [1..r-1], a <- [1..b-1]]

findFrac :: [Ratio Int] -> Int -> Int -> Int -> IO (Ratio Int)
findFrac xs index lowerIndex upperIndex 
  | abs (lowerIndex - upperIndex) <= 1 = return $ xs !! index
  | otherwise = do
      printf "is your fraction less than %s? (y/n/a=accept)" (show . (!!index) $ xs)
      resp <- getLine
      case resp of "y" -> let nIndex = round . (/2) . fromIntegral $ index+lowerIndex
                          in  findFrac xs nIndex lowerIndex index
                   "n" -> let nIndex = round . (/2) . fromIntegral $ index+upperIndex
                          in  findFrac xs nIndex index upperIndex
                   "a" -> return $ xs !! index
                   _   -> findFrac xs index lowerIndex upperIndex

solution :: [Ratio Int] -> IO (Ratio Int)
solution fs = findFrac fs (round $ fromIntegral (length fs) / 2.0) 0 (length fs - 1)

main = do
  hSetBuffering stdout NoBuffering
  s <- join $ (solution . generateFractions . (\(r:_) -> read r :: Int)) <$> getLine
  printf "your fraction is %s\n" (show s)
