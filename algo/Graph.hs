{-talking about dat graph theory {{{ -}

{-
bridges of Konigsberg
 
to get a solution, you cannot have more than 2 nodes with odd degree
degree: number of edges connected to a node

and you have to start from the odd-degree node

there are always a pair of odd numbers

Euler solved this:

+-+
|X|
+-+

no solution


if A->B is different from B->A => directed graph
if order does not matter => undirected graph

if you can travel from any two points => connected graph
if not => unconnected graph

a connected acyclic graph is called a tree
cyclic graph is when you can go from a node and come back to the same node

Graph = {SV, E}
SV = {vertex}
E = {(x, y) | x, y ∈ SV}

can have weighted graph

represent graphs:
  adjacency matrix (most common):
    
      A B C D
    A - 1 1 1
    B 1 - 1 0
    C 1 1 - 1
    D 1 0 1 -

    in undirected graph, only half of the matrix is useful

traverse tree level-order
  1. Queue q
  2. q.enqueue(root)
  3.
    1. x = q.dequeue()
    2. list x
    3. enqueue (all children of x)
    4. go to 3.1

graph traversal
  depth first
    use stack
  breadth first
    use queue

spanning tree:
  a tree that covers all the vertices of the graph

minimal spanning tree:
  spanning tree that minimizes costs in a weighted graph

shortest path:
  cheapest path between two points
}}} -}

module Graph (
  fromList,
  toList,
  traverseBreadthFirst,
  traverseDepthFirst
  ) where

import Data.List
import qualified Data.Map as M

-- Definitions and datatypes {{{
data Node a = Node { value :: a }
              deriving Eq

data Edge a = Edge { leftNode :: Node a
                   , rightNode :: Node a
                   , cost :: Int
                   } deriving Eq

instance Show a => Show (Node a) where
  show (Node x) = "N(" ++ show x ++ ")"

instance Show a => Show (Edge a) where
  show e = "E["
    ++ show (value $ leftNode e) ++ "=>" ++ show (value $ rightNode e)
    ++ " (" ++ show (cost e) ++ ")]"

newtype AdjMatrix a = AdjMatrix { graph :: [(Node a, [Node a])] }
                      deriving Show

fromList :: [(a, [a])] -> AdjMatrix a
fromList = AdjMatrix . fmap (\(x, ys) -> (Node x, map Node ys))

toList :: AdjMatrix a -> [(a, [a])]
toList = map (\(node, links) -> (unwrapNode node, unwrapLinks links)) . graph
  where
  unwrapNode (Node x) = x
  unwrapLinks links = map (\(Node x) -> x) links

type ConnectedGraph a = [Edge a]

-- }}}
-- Traversal algorithm {{{
data Traversal = Breadth | Depth

traverseHelper :: Eq a => Traversal -> AdjMatrix a -> [Node a]
traverseHelper t (AdjMatrix xs) = traverse' t [] xs
  where
  traverse' :: Eq a => Traversal
                       -> [Node a]
                       -> [(Node a, [Node a])]
                       -> [Node a]
  traverse' _ acc [] = reverse acc
  traverse' t acc ((node, links):xs)
    | node `elem` acc = traverse' t 
                                  acc
                                  ((case t of Breadth -> (++)
                                              Depth -> flip (++)) xs rest)
    | otherwise       = traverse' t
                                  (node:acc)
                                  ((case t of Breadth -> (++)
                                              Depth -> flip (++)) xs rest)
      where
      rest = filter (\(n, l) -> n `elem` links && n `notElem` acc) xs

-- choose same generation over decendent
traverseBreadthFirst :: Eq a => AdjMatrix a -> [Node a]
traverseBreadthFirst = traverseHelper Breadth

-- choose decendent over same generation
traverseDepthFirst :: Eq a => AdjMatrix a -> [Node a]
traverseDepthFirst = traverseHelper Depth

-- }}}
-- Shortest path (Dijkstra's algorithm) {{{

-- }}}
-- Minimum spanning tree {{{
-- smallest tree that covers all points
--
-- add the lightest edge which would not create a circle
-- keep adding randomly (the lightest)
-- stop when found n-1 edges (n = # of nodes)
-- }}}
-- Test graphs {{{
testMatrix = fromList [('a', "cb")
                      ,('b', "aed")
                      ,('c', "ad")
                      ,('d', "bcf")
                      ,('e', "")
                      ,('f', "")]

testConnectedGraph = fromList [('A', "BG")
                              ,('B', "CEA")
                              ,('G', "AEH")
                              ,('E', "BGF")
                              ,('F', "CEH")
                              ,('C', "BFD")
                              ,('H', "GFD")
                              ,('D', "CH")]

finalMatrix = fromList [('A', "BCD")
                       ,('B', "AHG")
                       ,('C', "AF")
                       ,('D', "AE")
                       ,('E', "D")
                       ,('F', "CG")
                       ,('G', "BF")
                       ,('H', "B")]

-- }}}
