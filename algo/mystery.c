#include <stdio.h>

int mystery(int a, int b) {
  if (b < a) {
    return 0;
  }
  
  return(1 + mystery(a-b, b));
}

int main() {
  printf("mystery(6, 2): %d\n", mystery(6 , 2));
  printf("mystery(30,7): %d\n", mystery(30, 7));
  printf("mystery(5,17): %d\n", mystery(5 ,17));
  printf("mystery(2,91): %d\n", mystery(2 ,91));
  return(0);
}
