public class merge {
  public static final boolean DEBUG = true;

  public static void merge(char[] a, int lo, int mid, int hi) {
    char[] aux = new char[a.length];

    // copy first half in ascending order
    for (int k = lo; k <= mid; k++) {
      aux[k] = a[k];
    }

    // copy second half in descending order
    int m = hi;
    int n = mid+1;
    while (m >= mid+1 && n <= hi) {
      aux[n++] = a[m--];
    }

    if (DEBUG) {System.out.print("a:  "); printArray(a);  }
    if (DEBUG) {System.out.print("aux:"); printArray(aux);}

    // now we don't need the boundary checks
    int i = lo, j = hi, pos = lo;
    while (i != j && !(pos > hi)) {
      if (aux[j] < aux[i]) {
        a[pos] = aux[j--];
      } else {
        a[pos] = aux[i++];
      }

      pos++;
    }

  }

  public static void main (String[] args) {
    String s = "EEGMRACERTYZ";
    char[] a = s.toCharArray();
    merge(a, 0, 4, 11);

    System.out.println("Solution:");
    printArray(a);
  }

  public static boolean less(Comparable v, Comparable w) {
    return v.compareTo(w) < 0;
  }

  public static void printArray(char[] a) {
    for (int i = 0; i < a.length; i++) {
      System.out.print(a[i] + " ");
    }
    System.out.println();
  }
}
