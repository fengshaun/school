-- guessing game
-- version 1
-- less accurate but faster
-- assuming 1/2 is equal to 2/4 is equal to 3/6 etc.
-- invocation: Guess [range]

module Main where

import System.Environment
import Text.Printf (printf)
import System.IO 

data Frac = Frac Integer Integer
num (Frac a b) = a
den (Frac a b) = b

instance Show Frac where
  show (Frac 0 _) = "0"
  show (Frac a b) = show a ++ "/" ++ show b

reduceFrac :: Frac -> Frac
reduceFrac (Frac a b) = Frac (a `quot` d) (b `quot` d)
  where
  d = gcd a b

findFrac :: Frac -> Frac -> Frac -> IO Frac
findFrac f@(Frac a b) lowerFrac upperFrac
  | abs (num lowerFrac - num upperFrac) <= 1 = return . reduceFrac $ f
  | otherwise = do
      printf "lower: %s upper: %s\n" (show lowerFrac) (show upperFrac)
      printf "Is your fraction smaller than %s? (y/n/a=accept)" (show . reduceFrac $ f)
      resp <- getLine
      case resp of "y" -> let newFrac = Frac (round $ (fromInteger $ a + num lowerFrac) / 2) b
                          in  findFrac newFrac lowerFrac (Frac a b)
                   "n" -> let newFrac = Frac (round $ (fromInteger $ a + num upperFrac) / 2) b
                          in  findFrac newFrac (Frac a b) upperFrac
                   "a" -> return . reduceFrac $ f
                   _   -> findFrac f lowerFrac upperFrac

solution :: Integer -> IO Frac
solution r = findFrac (Frac (round $ (fromInteger (r^2)) / 2) (r^2)) (Frac 0 (r^2)) (Frac (r^2) (r^2))

main = do
  hSetBuffering stdout NoBuffering
  args <- getArgs 
  let r = (read :: String -> Integer) . (!!0) $ args

  putStrLn $ "maximum fraction resolution is " ++ (show . Frac 1 . (^2) $ r)

  frac <- solution r 
  putStrLn $ "your fraction is " ++ show frac
