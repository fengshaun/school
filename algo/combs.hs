import Data.List

perm :: (Eq a) => [a] -> [[a]]
perm [] = [[]]
perm xs = [a : b | a <- xs, b <- perm (xs \\ [a]) ]

comb :: Int -> [a] -> [[a]]
comb 0 _ = [[]]
comb n xs = [a : b | a <- xs, b <- comb (n-1) xs]

