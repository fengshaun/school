/* Ring buffer implementation
 * for algo midterm #2
 */

public class RingBuffer<T> {
  private int front = 0;
  private int back = 0;
  private int count = 0;
  private int size;
  private T[] items;

  @SuppressWarnings({"unchecked"})
  public RingBuffer(int size) {
    this.items = (T[]) new Object[size];
    this.size = size;
  }

  public void enqueue(T i) {
    if (!isFull()) {
      items[back++] = i;
      if (back == size) {
        back = 0;
      }
      count++;
    }
  }

  public T dequeue() {
    if (isEmpty()) return null;

    T temp;
    temp = items[front++];
    if (front == size) {
      front = 0;
    }
    count--;
    return temp;
  }

  public boolean isEmpty() {
    return count == 0;
  }

  public boolean isFull() {
    return count == size;
  }
}
