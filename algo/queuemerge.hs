import Control.Monad.State

newtype Queue a = Queue [a]

instance Show a => Show (Queue a) where
  show (Queue xs) = "Queue " ++ show xs

enqueue :: a -> State (Queue a) ()
enqueue x = state $ \(Queue xs) -> ((), Queue (x:xs))

dequeue :: State (Queue a) a
dequeue = state $ \(Queue xs) -> (last xs, Queue (init xs))

peak :: State (Queue a) a
peak = state $ \(Queue xs) -> (last xs, Queue xs)

merge :: (Ord a) => Queue a -> Queue a -> Queue a
merge q1 q2 = mergeQueues q1 q2 (Queue [])
  where
  mergeQueues :: (Ord a) => Queue a -> Queue a -> Queue a -> Queue a
  mergeQueues (Queue []) (Queue []) acc = acc

  mergeQueues q1@(Queue []) q2 acc = let (a, q2')  = runState dequeue q2
                                         (_, acc') = runState (enqueue a) acc
                                     in  mergeQueues q1 q2' acc'

  mergeQueues q1 q2@(Queue []) acc = let (a, q1')  = runState dequeue q1
                                         (_, acc') = runState (enqueue a) acc
                                     in  mergeQueues q1' q2 acc'

  mergeQueues q1 q2 acc = let (a, _)    = runState peak q1
                              (b, _)    = runState peak q2
                              (_, acc') = if a > b 
                                            then runState (enqueue b) acc 
                                            else runState (enqueue a) acc
                              (_, q1')  = if a > b
                                            then runState peak q1 -- dummy expr
                                            else runState dequeue q1
                              (_, q2')  = if a > b
                                            then runState dequeue q2
                                            else runState peak q2 -- dummy expr
                          in mergeQueues q1' q2' acc'
