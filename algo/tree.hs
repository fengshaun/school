data Tree a = Tip | Node a (Tree a) (Tree a) deriving (Show)

leaf :: a -> Tree a
leaf x = Node x Tip Tip

left :: Tree a -> Tree a
left Tip = Tip
left (Node _ l _) = l

right :: Tree a -> Tree a
right Tip = Tip
right (Node _ _ r) = r

sampleTree :: Tree Int
sampleTree = Node 10 (Node 5 (leaf 2) (leaf 7)) (Node 100 Tip (leaf 110))

