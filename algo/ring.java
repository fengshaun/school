public class ring {
  public static void main (String[] args) {
    @SuppressWarnings({"unchecked"})
    RingBuffer<Integer> buf = new RingBuffer(3);
    buf.enqueue(5);
    buf.enqueue(7);
    buf.enqueue(9);

    if (buf.isFull()) {
      System.out.println("is full");
    }

    System.out.println(buf.dequeue());
    System.out.println(buf.dequeue());
    buf.enqueue(10);
    buf.enqueue(11);
    System.out.println(buf.dequeue());
    buf.enqueue(12);
    System.out.println(buf.dequeue());
    System.out.println(buf.dequeue());
    System.out.println(buf.dequeue());

    if (buf.isEmpty()) {
      System.out.println("is empty");
    }
  }
}
