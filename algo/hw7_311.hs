import qualified Data.Map as M

table :: M.Map String Double
table = M.fromList [("A+", 4.33)
                   ,("A" , 4.00)
                   ,("A-", 3.67)
                   ,("B+", 3.33)
                   ,("B" , 3.00)
                   ,("B-", 2.67)
                   ,("C+", 2.33)
                   ,("C" , 2.00)
                   ,("C-", 1.67)
                   ,("D" , 1.00)
                   ,("F" , 0.00)]

problem311 :: String -> String
problem311 xs = let grades = map (table M.!) . words $ xs
                    gpa = sum grades / fromIntegral (length grades) 
                in  "GPA is " ++ show gpa

main = interact problem311
