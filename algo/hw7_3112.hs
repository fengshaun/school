import Control.Monad
import Data.List (splitAt)

data Item a b = Item { key   :: a
                     , value :: b
                     } deriving Show

instance (Eq a) => Eq (Item a b) where
  x == y = key x == key y

instance (Ord a) => Ord (Item a b) where
  x <= y = key x <= key y

fromTuple :: (a, b) -> Item a b
fromTuple (x, y) = Item x y

splitAtMiddle :: [a] -> ([a], [a])
splitAtMiddle xs = splitAt (round . (/2) . fromIntegral . length $ xs) xs

binarySearch :: Ord a => a -> [Item a b] -> Maybe b
binarySearch _ [] = Nothing
binarySearch x xs
  | x == key midElem = Just $ value midElem
  | x <  key midElem = binarySearch x firstHalf
  | x >  key midElem = binarySearch x secondHalf
    where 
    s = splitAtMiddle xs
    firstHalf = fst s
    secondHalf = tail $ snd s
    midElem = head $ snd s

merge :: Ord a => [Item a b] -> [Item a b] -> [Item a b]
merge [] ys = ys
merge xs [] = xs
merge (x:xs) (y:ys)
  | x <= y = x:merge xs (y:ys)
  | x >  y = y:merge (x:xs) ys

mergeSort :: Ord a => [Item a b] -> [Item a b]
mergeSort [] = []
mergeSort [x] = [x]
mergeSort xs = merge (mergeSort $ fst s) (mergeSort $ snd s)
  where
  s = splitAtMiddle xs
