#include <stdio.h>

int f(int N) {
  int sum = 0;
  int n;
  for(n = 0; n < N; n += (N/2)) {
    sum++;
  }
  return(sum);
}

int ff(int N) {
  printf("f(%d): %d\n", N, f(N));
}

int main() {
  ff(101);
  ff(201);
  ff(399);
  ff(799);
  ff(1600);
}
