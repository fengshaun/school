data Item a b = Item { key   :: a
                     , value :: b
                     }

instance (Eq a) => Eq (Item a b) where
  x == y = key x == key y

instance (Ord a) => Ord (Item a b) where
  x <= y = key x <= key y

instance (Show a, Show b) => Show (Item a b) where
  show (Item key val) = "{" ++ show key ++ " => " ++ show val ++ "}"

fromTuple :: (a, b) -> Item a b
fromTuple (x, y) = Item x y

splitAtMiddle :: [a] -> ([a], [a])
splitAtMiddle xs = splitAt (round . (/2) . fromIntegral . length $ xs) xs

binaryDelete :: Ord a => a -> [Item a b] -> [Item a b]
binaryDelete _ [] = []
binaryDelete x xs
  | x <  key midElem = binaryDelete x firstHalf ++ secondHalf
  | x >  key midElem = firstHalf
                         ++ [midElem] 
                         ++ binaryDelete x (tail secondHalf)
  | x == key midElem = firstHalf ++ tail secondHalf
    where
    s = splitAtMiddle xs
    firstHalf = fst s
    secondHalf = snd s
    midElem = head $ snd s
