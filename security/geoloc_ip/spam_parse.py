import urllib.request
import json

# have to use cp1250 since the file was created on windows
source = []
with open("spam_ip.csv", "r", encoding="utf-16-le") as f:
    source = f.readlines()

with open("spam_ip.csv", "r", encoding="utf-16-le") as f:
    with open("spam_result.csv", "a") as f2:
        for ip in f.readlines():
            ip = ip.strip("\ufeff").strip("\n")
            res = json.loads(bytes.decode(urllib.request.urlopen("http://10.10.120.100:8080/%s/%s" % ("json",ip)).read()).rstrip())
            f2.write("%s,%s,%s\n" % (ip, res["latitude"],res["longitude"]))
