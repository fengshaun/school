/* Program: fork.c -- a Simple fork
 * shows PID, PPID in both parent and child
 */

#include <stdio.h>
#include <stdlib.h>

int main() {
    printf("Before fork\n");
    pid_t pid = fork();

    if (pid > 0) {
        sleep(1);
        printf("PARENT -- PID: %d PPID: %d, CHILD PID: %d\n", getpid(), getppid(), pid);
    } else if (pid == 0) {
        printf("CHILD -- PID: %d PPID: %d\n", getpid(), getppid());
    } else {
        printf("Fork error\n");
        exit(1);
    }
    
    printf("Both processes continue from here\n");
    exit(0);
}

