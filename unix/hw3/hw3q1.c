/*
 * Program filename:  *** type the actual filename you chose here like in the textbook. ***
 *
 * Program Description:
 * This program reads the content of a given directory and displays it.
 * Uses the system call function opendir( ) to open the directory file.
 * Uses the system call function readdir( ) to read the directory content one file entry at time
 * in a the while loop until there is no more entry (end of file).
 * Finally, after reading all the entries, the directory file is closed using the 
 * system call function closedir( ).
 *
 * Function: main ( )
 * Input parameter:
 *	Directory name
 * Output parameter:
 *	None
 *
 * Return value:
 *	 0, if successful
 *	-1, if a directory name is not given as argument
 *	-2, if opening the directory file (opendir) fails
 *
 * Program Defect:
 *	This program does not explicitly check for readdir system call failure.
 *	But returns successful status.
 */

#include <dirent.h>		/* For DIR and struct dirent */
#include <stdio.h>		/* For standard input and output */
#include <stdlib.h>

int main (
int 	argc, 		/* Number of arguments supplied when running the program */
char 	**argv		/* Values of arguments */
)
{
	DIR *dir;			/* Directory file pointer returned by opendir */
	struct dirent	*direntry;	/* Information for one file in the directory */
/* returned by readir */

	/* Check number of arguments supplied */
	if (argc != 2)
	{
		printf("Run the program specifying one directory name as argument.\n");
		exit (-1);	/* return error status */
	}

	/* open the directory file */
if ((dir = opendir(argv[1])) == NULL)
{
		printf("%s directory file open failed.\n", argv[1]);
		exit (-2);	/* return error status */
	}

	/* Read directory entries one file entry at a time until end of file or error */
	while ((direntry = readdir(dir)) != NULL)	/* until entries are exhausted */
	{
		printf ("%10d %s\n", (int)direntry->d_ino,  direntry->d_name);
	}

	/* Close directory file */
	closedir (dir);

	exit (0);  /* return successful status */
}  /* end main ( ) */
