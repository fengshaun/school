Script started on Thu 27 Sep 2012 06:04:12 PM EDT
$ echo "HW2 Question 7"
HW2 Question 7
$ chmod +x q7.sh
$ ls -l q7.sh 
-rwxrwxr-x 1 armin armin 126 Sep 27 18:03 q7.sh
$ cat q7.sh 
# display the first 12 arguments

count=0

while [ $count -lt 12 ]; do
    echo $1
    count=`expr $count + 1`
    shift
done
$ ./q7.sh hello world this is a test arg and should be more than twelve args in total
hello
world
this
is
a
test
arg
and
should
be
more
than
$ exit
exit

Script done on Thu 27 Sep 2012 06:06:30 PM EDT
