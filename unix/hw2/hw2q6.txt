Script started on Thu 27 Sep 2012 05:53:56 PM EDT
$ echo "HW2 Question 6"
HW2 Question 6
$ chmod +x q6.sh
$ ls -l q6.sh
-rwxrwxr-x 1 armin armin 212 Sep 27 17:53 [0m[01;32mq6.sh[0m
$ cat q6.sh
#!/bin/bash

sleep 1500 &
echo $!
ps
myvar=$!
cat nonexistent

if [[ "$?" == "0" ]]; then
	echo "cat command completed successfully"
else
	echo "cat command failed"
fi

kill $myvar

ps

echo "the script is done"
$ ./q6.sh
3941
  PID TTY          TIME CMD
 3828 pts/6    00:00:00 bash
 3940 pts/6    00:00:00 q6.sh
 3941 pts/6    00:00:00 sleep
 3942 pts/6    00:00:00 ps
cat: nonexistent: No such file or directory
cat command failed
  PID TTY          TIME CMD
 3828 pts/6    00:00:00 bash
 3940 pts/6    00:00:00 q6.sh
 3947 pts/6    00:00:00 ps
./q6.sh: line 17:  3941 Terminated              sleep 1500
the script is done
$ exit
exit

Script done on Thu 27 Sep 2012 05:55:07 PM EDT
