Script started on Wed 26 Sep 2012 05:38:33 PM EDT
$ echo "HW2 Question 1"
HW2 Question 1
$ ps -u armin
  PID TTY          TIME CMD
 1657 ?        00:00:00 gnome-keyring-d
 1668 ?        00:00:00 gnome-session
 1703 ?        00:00:00 ssh-agent
 ...
 8191 ?        00:00:05 soffice.bin
 8278 pts/0    00:00:00 script
 8280 pts/5    00:00:00 bash
 8338 pts/5    00:00:00 ps
$ ps -l
F S   UID   PID  PPID  C PRI  NI ADDR SZ WCHAN  TTY          TIME CMD
0 S  1000  8280  8279  0  80   0 -  5587 wait   pts/5    00:00:00 bash
0 R  1000  8384  8280  0  80   0 -  2110 -      pts/5    00:00:00 ps
$ echo "name : bash, state: S"
name: bash, state: S
$ ps -u armin -l
F S   UID   PID  PPID  C PRI  NI ADDR SZ WCHAN  TTY          TIME CMD
1 S  1000  1657     1  0  80   0 - 129462 poll_s ?       00:00:00 gnome-keyring-d
4 S  1000  1668  1479  0  80   0 - 97317 poll_s ?        00:00:00 gnome-session
0 S  1000  8278  7148  0  80   0 -  4089 n_tty_ pts/0    00:00:00 script
...many processes...
1 S  1000  8279  8278  0  80   0 -  4090 -      pts/0    00:00:00 script
0 S  1000  8280  8279  0  80   0 -  5587 wait   pts/5    00:00:00 bash
0 R  1000  8389  8280  0  80   0 -  4218 -      pts/5    00:00:00 ps
$ ps -l
F S   UID   PID  PPID  C PRI  NI ADDR SZ WCHAN  TTY          TIME CMD
0 S  1000  8280  8279  0  80   0 -  5587 wait   pts/5    00:00:00 bash
0 R  1000  8391  8280  0  80   0 -  2110 -      pts/5    00:00:00 ps
$ echo "name: bash, PPID: 8279"
name: bash, PPID: 8279
$ ps aux
USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
root         1  0.0  0.0  24428  2420 ?        Ss   14:34   0:01 /sbin/init
root         2  0.0  0.0      0     0 ?        S    14:34   0:00 [kthreadd]
root         3  0.0  0.0      0     0 ?        S    14:34   0:02 [ksoftirqd/0]
root         6  0.0  0.0      0     0 ?        S    14:34   0:00 [migration/0]
root         7  0.0  0.0      0     0 ?        S    14:34   0:01 [watchdog/0]
...many processes...
armin     8280  0.0  0.1  22348  4896 pts/5    Ss   17:38   0:00 bash -i
root      8336  0.0  0.0      0     0 ?        S    17:38   0:00 [kworker/1:0]
root      8337  0.0  0.0      0     0 ?        S    17:38   0:00 [kworker/0:2]
root      8340  0.0  0.0      0     0 ?        S    17:38   0:00 [kworker/3:1]
root      8348  0.0  0.0      0     0 ?        S    17:39   0:00 [kworker/u:0]
root      8372  0.1  0.5 126084 20392 ?        SN   17:41   0:00 /usr/bin/python /usr/sbin/aptd
armin     8395  0.0  0.0  16872  1276 pts/5    R+   17:43   0:00 ps aux
$ exit
exit

Script done on Wed 26 Sep 2012 05:44:01 PM EDT
Script started on Wed 26 Sep 2012 05:49:19 PM EDT
$ echo "HW2 Question 2"
HW2 Question 2
$ cd
$ pwd
/home/armin
$ who | wc -l
2
$ who | wc -w
9
$ cat /etc/passwd
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/bin/false
daemon:x:2:2:daemon:/sbin:/bin/false
mail:x:8:12:mail:/var/spool/mail:/bin/false
ftp:x:14:11:ftp:/srv/ftp:/bin/false
http:x:33:33:http:/srv/http:/bin/false
nobody:x:99:99:nobody:/:/bin/false
dbus:x:81:81:System message bus:/:/bin/false
armin:x:1000:100::/home/armin:/bin/zsh
avahi:x:84:84:avahi:/:/bin/false
mpd:x:45:45::/var/lib/mpd:/bin/false
git:x:999:999:git daemon user:/:/bin/bash
uuidd:x:68:68::/:/sbin/nologin
$ cat /etc/passwd | wc -c
1661
$ ls -l /etc/passwd
-rw-r--r-- 1 root root 1661 Sep 11 16:51 /etc/passwd
$ echo "the bytes shown by wc and ls -l are the same"
the bytes shown by wc and ls -l are the same
$ wc < /etc/hosts
 12  29 270
$ exit
exit

Script done on Wed 26 Sep 2012 05:52:49 PM EDT
Script started on Wed 26 Sep 2012 05:53:39 PM EDT
$ echo "HW2 Question 3"
HW2 Question 3
$ ps
  PID TTY          TIME CMD
 8680 pts/2    00:00:00 bash
 8738 pts/2    00:00:00 ps
$ sleep 1500 &
[1] 8740
$ jobs
[1]+  Running                 sleep 1500 &
$ ps
  PID TTY          TIME CMD
 8680 pts/2    00:00:00 bash
 8740 pts/2    00:00:00 sleep
 8742 pts/2    00:00:00 ps
$ ps -l
F S   UID   PID  PPID  C PRI  NI ADDR SZ WCHAN  TTY          TIME CMD
0 S  1000  8680  8679  0  80   0 -  5587 wait   pts/2    00:00:00 bash
0 S  1000  8740  8680  0  80   0 -  1468 hrtime pts/2    00:00:00 sleep
0 R  1000  8744  8680  0  80   0 -  2110 -      pts/2    00:00:00 ps
$ echo "character for state of sleep command: S"
character for state of sleep command: S
$ kill 8740
$ ps
  PID TTY          TIME CMD
 8680 pts/2    00:00:00 bash
 8751 pts/2    00:00:00 ps
[1]+  Terminated              sleep 1500
$ exit
exit

Script done on Wed 26 Sep 2012 05:55:55 PM EDT
Script started on Wed 26 Sep 2012 05:57:30 PM EDT
$ echo "HW2 Question 4"
HW2 Question 4
$ echo $SHELL
/bin/bash
$ echo "the name of the shell command is /bin/bash"
the name of the shell command is /bin/bash
$ echo $HOME
/home/armin
$ echo "the HOME variable stands for the login directory"
the HOME variable stands for the login directory
$ echo $PATH
/usr/lib/lightdm/lightdm:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games
$ echo "PATH does not contain . and does not search the current directory"
PATH does not contain . and does not search the current directory
$ echo $MAIL
/var/spool/mail
$ echo "system stores mail in /var/spool/mail"
system stores mail in /var/spool/mail
$ echo $TERM
screen-256color
$ echo "I'm using a screen-256color terminal"
I'm using a screen-256color terminal
$ cat ~/.profile
# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi
$ echo $BASH_VERSION
4.2.24(1)-release
$ echo $HOME
/home/armin
$ echo $PATH
/usr/lib/lightdm/lightdm:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games
$ exit
exit

Script done on Wed 26 Sep 2012 06:03:31 PM EDT
Script started on Wed 26 Sep 2012 06:09:28 PM EDT
$ echo "HW2 Question 5"
HW2 Question 5
$ chmod a+x birthday
$ ls -l birthday
-rwxrwxr-x 1 armin armin 133 Sep 26 18:09 birthday
$ cat birthday
grep -i $1 << HERE
Mary    June 2
Tom     Feb 10
Jim     April 25
Joe     April 20
Deb     Aug 12
Julie   Dec 25
John    Oct 7y
HERE
$ ./birthday tom
Tom     Feb 10
$ ./birthday 25
Jim     April 25
Julie   Dec 25
$ ./birthday bambam
$ exit
exit

Script done on Wed 26 Sep 2012 06:11:05 PM EDT
Script started on Thu 27 Sep 2012 05:53:56 PM EDT
$ echo "HW2 Question 6"
HW2 Question 6
$ chmod +x q6.sh
$ ls -l q6.sh
-rwxrwxr-x 1 armin armin 212 Sep 27 17:53 [0m[01;32mq6.sh[0m
$ cat q6.sh
#!/bin/bash

sleep 1500 &
echo $!
ps
myvar=$!
cat nonexistent

if [[ "$?" == "0" ]]; then
	echo "cat command completed successfully"
else
	echo "cat command failed"
fi

kill $myvar

ps

echo "the script is done"
$ ./q6.sh
3941
  PID TTY          TIME CMD
 3828 pts/6    00:00:00 bash
 3940 pts/6    00:00:00 q6.sh
 3941 pts/6    00:00:00 sleep
 3942 pts/6    00:00:00 ps
cat: nonexistent: No such file or directory
cat command failed
  PID TTY          TIME CMD
 3828 pts/6    00:00:00 bash
 3940 pts/6    00:00:00 q6.sh
 3947 pts/6    00:00:00 ps
./q6.sh: line 17:  3941 Terminated              sleep 1500
the script is done
$ exit
exit

Script done on Thu 27 Sep 2012 05:55:07 PM EDT
Script started on Thu 27 Sep 2012 06:04:12 PM EDT
$ echo "HW2 Question 7"
HW2 Question 7
$ chmod +x q7.sh
$ ls -l q7.sh 
-rwxrwxr-x 1 armin armin 126 Sep 27 18:03 q7.sh
$ cat q7.sh 
# display the first 12 arguments

count=0

while [ $count -lt 12 ]; do
    echo $1
    count=`expr $count + 1`
    shift
done
$ ./q7.sh hello world this is a test arg and should be more than twelve args in total
hello
world
this
is
a
test
arg
and
should
be
more
than
$ exit
exit

Script done on Thu 27 Sep 2012 06:06:30 PM EDT
Script started on Mon 01 Oct 2012 03:08:09 PM EDT
$ echo "HW2 Question 8"
HW2 Question 8
$ cat hw2test1
Martin
Judy
Ruth
Levi
Alicia
Alicia
Alicia
Alice
Charles
Minnie
Minnie
John
Terry
Margaret
$ cat hw2test2
Ruth
Levi
Troy
Troy
Joseph
Minnie
John
John
Terry
John
Zachary
$ cat hw2test3
Martin
Judy
Ruth
Levi
Alicia
Alicia
Alicia
Alice
Charles
Minnie
Minnie
John
Terry
Margaret
$ cmp hw2test1 hw2test2
hw2test1 hw2test2 differ: byte 1, line 1
$ diff hw2test1 hw2test2
1,2d0
< Martin
< Judy
5,10c3,5
< Alicia
< Alicia
< Alicia
< Alice
< Charles
< Minnie
---
> Troy
> Troy
> Joseph
12a8
> John
14c10,11
< Margaret
---
> John
> Zachary
$ sort hw2test1 > hw2test1sorted
$ sort hw2test2 > hw2test2sorted
$ comm hw2test1sorted hw2test2sorted 
Alice
Alicia
Alicia
Alicia
Charles
		John
	John
	John
	Joseph
Judy
		Levi
Margaret
Martin
		Minnie
Minnie
		Ruth
		Terry
	Troy
	Troy
	Zachary
$ tail -5 hw2test1
Minnie
Minnie
John
Terry
Margaret
$ head hw2test1
Martin
Judy
Ruth
Levi
Alicia
Alicia
Alicia
Alice
Charles
Minnie
$ uniq hw2test1
Martin
Judy
Ruth
Levi
Alicia
Alice
Charles
Minnie
John
Terry
Margaret
$ cat hw2test1
Martin
Judy
Ruth
Levi
Alicia
Alicia
Alicia
Alice
Charles
Minnie
Minnie
John
Terry
Margaret
$ touch hw2_before_setting_umask
$ umask 600
$ touch hw2_after_setting_umask
$ ls -l
total 20
-rwxrwxr-x 1 armin armin  133 Sep 26 18:09 birthday
----rw-rw- 1 armin armin    0 Oct  1 15:19 hw2_after_setting_umask
-rw-rw-r-- 1 armin armin    0 Oct  1 15:19 hw2_before_setting_umask
-rw-rw-r-- 1 armin armin 2623 Sep 26 18:12 hw2q1.txt
-rw-rw-r-- 1 armin armin  456 Sep 26 18:12 hw2q2.txt
-rw-rw-r-- 1 armin armin  980 Sep 26 18:12 hw2q3.txt
-rw-rw-r-- 1 armin armin 1793 Sep 26 18:12 hw2q4.txt
-rw-rw-r-- 1 armin armin  508 Sep 26 18:12 hw2q5.txt
-rw-rw-r-- 1 armin armin  883 Sep 27 17:56 hw2q6.txt
-rw-rw-r-- 1 armin armin  498 Sep 27 18:07 hw2q7.txt
-rw-rw-r-- 1 armin armin 2048 Oct  1 15:18 hw2q8.txt
-rw-rw-r-- 1 armin armin   91 Oct  1 15:18 hw2test1
-rw-rw-r-- 1 armin armin   91 Oct  1 15:09 hw2test1sorted
-rw-rw-r-- 1 armin armin   63 Sep 27 18:19 hw2test2
-rw-rw-r-- 1 armin armin   63 Oct  1 15:09 hw2test2sorted
-rw-rw-r-- 1 armin armin   91 Oct  1 15:08 hw2test3
-rwxrwxr-x 1 armin armin  212 Sep 27 17:53 q6.sh
-rwxrwxr-x 1 armin armin  126 Sep 27 18:03 q7.sh
$ echo "read and write permissions for user were removed due to the above umask command"
read and write permissions for user were removed due to the above umask command
$ grep "Alicia" hw2test1
Alicia
Alicia
Alicia
$ grep "Ruth" hw2test{1,2}
hw2test1:Ruth
hw2test2:Ruth
$ grep "Joseph" hw2test{1,2,3}
hw2test2:Joseph
$ exit
exit

Script done on Mon 01 Oct 2012 03:23:22 PM EDT
