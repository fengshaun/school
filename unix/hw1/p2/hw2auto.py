#!/usr/bin/python

import subprocess as P
import os

cmds = """ admin
at
awk
bg
cal
calendar
cat
cc
cd
chgrp
chmod
chown
chsh
comm
compress
cp
cpio
crontab
date
dbx
delta
deroff
df
diff
du
echo
expr
fg
file
find
finger
fsck
ftp
get
getty
grep
head
hostname
init
kill
learn
lex
ln
lp
lpq
lpr
ls
mail
mailx
make
man
mesg
mkdir
more
mount
mv
newgrp
news
nice
nohup
od
passwd
ping
pr
prs
ps
pwd
rcp
rlogin
rm
rmdel
rmdir
rsh
ruptime
rwho
sed
set
shift
shutdown
sleep
sort
spell
stty
su
tail
talk
tar
tee
telnet
test
touch
tr
tty
umask
umount
uncompress
uniq
unset
vi
view
wall
wc
what
whereis
which
who
write
zcat""".split("\n")

template = "%s\t%s\t\t%s"

for seq in range(len(cmds)):
  desc = ""

  if os.system("man %s 2> /dev/null > temp" % cmds[seq]) != 0:
    desc = "NO MANPAGE: %s" % cmds[seq]
  else:
    with open("temp", "r") as f:
      lines = f.readlines()

      for i in range(len(lines)):
        if "NAME" in lines[i]:
          desc = "".join(lines[i+1].split("-")[1:]).strip().rstrip()
          break

  print template % (seq, cmds[seq], desc)

os.unlink("temp")
