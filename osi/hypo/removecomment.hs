import System.Environment
import Data.Text
import Text.Regex.Posix
import Control.Monad

main = do
  getArgs >>= readFile . head >>=
    map (unpack . stripEnd . head . split (== '#') . pack) .
    filter (not . null) .
    filter (not . (=~ "^#") :: Bool)
