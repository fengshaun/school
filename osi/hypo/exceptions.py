# Custom exception definitions
# These exceptions are used to signify not only possible errors, but also
# working states in the HYPO machine.  They are also used in rudimentary 
# message-passing as in the use of Halt exception.
# 
# signifies a halt instruction
# used in message-passing
class Halt(Exception):
  pass

class Suspend(Exception):
  def __init__(self, adj):
    self.pcAdjustment = adj

# signifies an Invalid Address error
class InvalidAddress(Exception):
  def __init__(self, addr, val):
    self.errno = 10
    self.strerror = "Error [{0}]: invalid address in line '{1} {2}'".format(
        self.errno, addr, val)

# syntax error in a specific line
# can't used SyntaxError since it's a reserved keyword
class SynError(Exception):
  def __init__(self, line):
    self.errno = 11
    self.strerror = "Error [{0}]: syntax error in line '{1}'.".format(
        self.errno, line)

# end of file reached without properly ending the program
class EOFReached(Exception):
  def __init__(self):
    self.errno = 12
    self.strerror = "Error [{0}]: end of file reached".format(self.errno)

# Program Counter is out of bounds and memory seems full
class MemoryOverflow(Exception):
  def __init__(self):
    self.errno = 20
    self.strerror = "Error [{0}]: Memory overflow, program not terminated".format(
        self.errno)

# Invalid opcode detected
class InvalidOpcode(Exception):
  def __init__(self, opcode):
    self.errno = 30
    self.strerror = "Error [{0}]: Invalid opcode '{1}' detected".format(
        self.errno, opcode)

# Invalid GPR Address
class InvalidGPRAddress(Exception):
  def __init__(self, gpr):
    self.errno = 31
    self.strerror = "Error [{0}]: Invalid GPR address '{1}' detected".format(
        self.errno, gpr)

# Invalid Operand mode
class InvalidOperandMode(Exception):
  def __init__(self, mode):
    self.errno = 32
    self.strerror = "Error [{0}]: Invalid operand mode '{1}' detected".format(
        self.errno, mode)

# Invalid address
class InvalidMemoryAddress(Exception):
  def __init__(self, addr):
    self.errno = 33
    self.strerror = "Error [{0}]: Invalid memory address '{1}'".format(
        self.errno, addr)

# Restricted memory access
# trying to access restricted part of the memory
class RestrictedMemoryAccess(Exception):
  # addr is the address and accessor is the user/OS trying to access the memory
  def __init__(self, addr, accessor):
    self.errno = 34
    self.strerror = """Error [{0}]: Trying to access restricted memory address
'{1}' as {2}""".format(self.errno, addr, accessor)

# Stack underflow
# trying to pop more than the stack has
class StackUnderflow(Exception):
  def __init__(self):
    self.errno = 40
    self.strerror = "Error [{0}]: Stack underflow error".format(self.errno)

# Stack overflow
# trying to push more than the stack limit
class StackOverflow(Exception):
  def __init__(self):
    self.errno = 41
    self.strerror = "Error [{0}]: Stack overflow error".format(self.errno)

#
# execution status codes as exceptions
#
class TimeSliceExpired(Exception):
  pass

class StartOfInput(Exception):
  pass

class StartOfOutput(Exception):
  pass

class WaitingForMessage(Exception):
  pass

class Shutdown(Exception):
  pass
