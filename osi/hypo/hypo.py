# Armin Moradi
# Homework #1
# Operating System Internals

# FIXED: error codes are in exceptions.py along with definitions of exceptions
# FIXED: program-level documentation added
# FIXED: The way python is often commented along with folding conventions of Vim
#        might make viewing this a bit harder on non-programming text editors.

# hypo.py - a HYPO machine simulator
# this program reads a hypo machine code file and runs the program specified by
# instructions in that file.
# it has interrupt handling and can run multiple programs simultaneously.
# run by issuing the following command:
# $ python hypo.py
# requires python 3.3 and above (although might work on python 3.0+ well)
# does not rely on any external libraries.

import sys
import queue
from exceptions import *

# utility functions for the HYPO machine {{{
# Function: validate_operand
# validates a given operand and makes sure it is within its bound limits
#
# input: operand
# output: none
def validate_operand(op):
  if op.mode < 0 or op.mode > 6:
    raise InvalidOperandMode(op.mode)
  elif op.gpr < 0 or op.gpr > 7:
    raise InvalidGPRAddress(op.gpr)
  elif op.addr < -1 or op.addr >= Hardware.RAM_LIMIT:
    raise InvalidMemoryAddress(op.addr)
  else:
    return op
# }}}
# Operand Class {{{
# represents an operand with attributes typically associated
# with it such as operand mode, GPR address, and if applicable, memory address
class Operand():
  # init function, sets the attributes of the operand class: mode and gpr
  def __init__(self, mode, gpr):
    # validation for this object is done outside this class
    self.mode = mode
    self.gpr = gpr
    
    # FIXED: self.adder initialized to -1
    self.addr = -1

    self.__set_addr()

  # get_value {{{
  # take no arguments and return the value of the operand.  It uses the address
  # saved when the operand was created.
  def get_value(self):
    if self.mode == 0:
      # let the instruction handle this
      # the instruction might not need the operand
      pass
    elif self.mode == 1:
      # register mode
      return Hardware.GPR[self.gpr]
    elif self.mode == 2:
      # register deferred
      return Hardware.RAM[self.addr]
    elif self.mode == 3:
      # autoincrement
      addr = Hardware.GPR[self.gpr]
      # FIXED:
      Hardware.GPR[self.gpr] += 1

      # FIXED:
      if addr < 9999 and addr > 0:
        return HardWare.RAM[addr]
      else:
        raise InvalidMemoryAddress(addr)
    elif self.mode == 4:
      # autodecrement
      # FIXED:
      Hardware.GPR[self.gpr] -= 1
      addr = Hardware.GPR[self.gpr]

      if addr < 9999 and addr > 0:
        return HardWare.RAM[addr]
      else:
        raise InvalidMemoryAddress(addr)
    elif self.mode == 5:
      # direct mode
      # FIXED: self.addr is set in __set_addr() and is a unified way for
      # get_value() to access the contents.  This is done to reduce scatter
      # and resulting bugs.  Internally, self.addr is set to PC.
      return Hardware.RAM[self.addr]
    elif self.mode == 6:
      # immediate mode
      return Hardware.RAM[self.addr]
    # }}}
  # set_value {{{
  # takes a value and assigns it to an operand
  # assigns the value to the GPR location or memory address of the operand
  def set_value(self, value):
    if self.mode == 0:
      # let the instruction handle this
      # the instruction might not need the operand
      pass
    elif self.mode == 1:
      # register mode
      # FIXED: the program works as intended.
      # this is a set_value operation on one operand, this operand does not
      # know about any other possible operands that might be in an instruction.
      # It lives in isolation, potentially preventing many bugs.
      Hardware.GPR[self.gpr] = value
    elif self.mode == 2:
      # register deferred
      Hardware.RAM[self.addr] = value
    elif self.mode == 3:
      # autoincrement
      addr = Hardware.GPR[self.gpr]
      self.gpr += 1
      Hardware.RAM[addr] = value
    elif self.mode == 4:
      # autodecrement
      self.gpr -= 1
      addr = Hardware.GPR[self.gpr]
      Hardware.RAM[addr] = value
    elif self.mode == 5:
      # direct mode
      Hardware.RAM[self.addr] = value
    elif self.mode == 6:
      # immediate mode
      Hardware.RAM[self.addr] = value
    # }}}
  # __set_addr {{{
  # takes no arguments and returns nothing.  This function only calculates and
  # sets the address of the operand.  It should not be called externally, hence
  # the name mangling through double underscore.
  def __set_addr(self):
    if self.mode == 0:
      # let the instruction handle this
      # the instruction might not need this operand
      pass

    if self.mode == 1:
      # register mode
      # no address to speak of
      # FIXED: changed 0 to -1
      self.addr = -1

    elif self.mode == 2:
      # register deferred
      self.addr = Hardware.GPR[self.gpr]

    elif self.mode == 3:
      # autoincrement
      # address changes
      # FIXED: self.gpr is enough to find the value and location of the operand.
      # __set_addr() should do as little work as possible.  Please see get_value()
      # and set_value() regarding how they interact.
      self.addr = -1

    elif self.mode == 4:
      # autodecrement
      # address changes
      # FIXED: self.gpr is enough to find the value and location of the operand.
      # __set_addr() should do as little work as possible.  Please see get_value()
      # and set_value() regarding how they interact.
      self.addr = -1

    elif self.mode == 5:
      # direct mode
      # FIXED: Hardware.PC is not the instruction location.  It has been incremented
      # previously and is now the location of the operand.
      self.addr = Hardware.RAM[Hardware.PC]
      Hardware.PC += 1
    elif self.mode == 6:
      # immediate mode
      # FIXED: Hardware.PC is not the instruction location.  It has been incremented
      # previously and is now the location of the operand.
      self.addr = Hardware.PC
      Hardware.PC += 1
    # }}}
  # __str__ {{{
  # return the string representation of this operand.
  def __str__(self):
    return "Operand(mode:{0} gpr:{1} addr:{2} value:{3})".format(
        self.mode, self.gpr, self.addr, self.get_value())
  # }}}
#}}}
# PCB Class {{{
# Represents a PCB with all of its attributes.  Each attribute is the real addr
# of the specified location.
class PCB:
  size = 20

  def __init__(self, start):
    # start is the starting memory address of the PCB region
    self.start = start
    self.nextPCB = start + 0
    self.pid = start + 1
    self.state = start + 2
    self.reasonForWaiting = start + 3
    self.priority = start + 4
    self.stackStart = start + 5
    self.stackSize = start + 6
    self.msgQueueStart = start + 7
    self.msgQueueSize = start + 8
    self.numberOfMsgs = start + 9
    self.gpr0 = start + 10
    self.gpr1 = start + 11
    self.gpr2 = start + 12
    self.gpr3 = start + 13
    self.gpr4 = start + 14
    self.gpr5 = start + 15
    self.gpr6 = start + 16
    self.gpr7 = start + 17
    self.SP = start + 18
    self.PC = start + 19

  def __lt__(self, other):
    return Hardware.RAM[self.priority] < Hardware.RAM[other.priority]
# }}}
# Message class {{{
class Message:
  def __init__(self, receiverPid, addr):
    self.receiverPid = receiverPid
    self.addr = addr
# }}}
# MemoryBlock class {{{
# Represents a Memory address block.  It can be either free or taken depending
# on where it appears.
class MemoryBlock:
  def __init__(self, start, size):
    self.start = start
    self.size = size
# }}}
# Hardware components represented as stateful class {{{
class Hardware():
  MAR = 0 # Memory Address Register
  MBR = 0 # Memory Buffer Register
  IR  = 0 # Instruction Register
  SP  = 0 # Stack Pointer
  PC  = 0 # Program Counter
  PSR = 0 # Processor Status Register
  CLOCK = 0 # System Clock

  # FIXED: Python does not have fixed size arrays, therefore, no fixed size
  # can be specified for GPR[] and RAM[].  They are initialized to their respective
  # sizes in initializeSystem().
  GPR = [] # General Purpose Registers
  RAM = [] # memory.  Limit: 10000
  RAM_LIMIT = 10000

  # 2 spots are always reserved at 0, 1, and 2 for the null process
  # and cannot be overwritten
  RAM_USER_LIMIT_MIN = 3
  RAM_USER_LIMIT_MAX = 1999
  RAM_STACK_LIMIT_MIN = 2000
  RAM_STACK_LIMIT_MAX = 5999
  RAM_OS_LIMIT_MIN = 6000
  RAM_OS_LIMIT_MAX = 9999

  END_ADDRESS = -1 # address signifying the end of program
# }}}
# Global components of the MTOPS {{{
class Globals:
  RQ = queue.PriorityQueue()
  WQ = []
  MQ = {}
  PID = 0
  DefaultPriority = 128
  ReadyState = 1
  WaitingState = 2
  EndOfList = -1
  osFreeList = [MemoryBlock(Hardware.RAM_OS_LIMIT_MIN,
                            Hardware.RAM_OS_LIMIT_MIN
                            - Hardware.RAM_OS_LIMIT_MAX)]
  userFreeList = [MemoryBlock(Hardware.RAM_USER_LIMIT_MIN,
                              Hardware.RAM_USER_LIMIT_MAX
                              - Hardware.RAM_USER_LIMIT_MIN)]
  TimeSlice = 200
  OSMode = 1
  UserMode = 2

  ReasonInput = 1
  ReasonOutput = 2
#}}}
# specific execution functions for each instruction {{{
# all of the instruction execution function are unified with the following
# function signature:
# void do_instruction_name(Operand op1, Operand op2);
#
# each function returns nothing and takes two Operands as arguments.
# the task of each function is documented before the definition of each function
#
# do_nothing {{{
# a place-holder function for instructions that do nothing
def do_nothing(op1, op2):
  pass
# }}}
# do_halt {{{
# raises a Halt exception to be caught where appropriate
# the Halt exception signifies a halt instruction.
def do_halt(op1, op2):
  raise Halt()
# }}}
# do_add {{{
# adds the value of two operands and stores the result into operand 1 location
def do_add(op1, op2):
  op1.set_value(op1.get_value() + op2.get_value())
# }}}
# do_subtract {{{
# subtracts the value of the second operand from the first operand and stores
# the result in operand 1 location
def do_subtract(op1, op2):
  op1.set_value(op1.get_value() - op2.get_value())
# }}}
# do_multiply {{{
# multiplies the values of the two operand and stores the result in operand 1
# location
def do_multiply(op1, op2):
  op1.set_value(op1.get_value() * op2.get_value())
# }}}
# do_divide {{{
# divides the first operand by the second operand and stores the result in
# operand 1 location
def do_divide(op1, op2):
  # this might throw ZeroDivisionError, we catch it in main
  # FIXED: // specifies floor division (i.e. what HYPO expects). / specifies
  # real division
  op1.set_value(op1.get_value() // op2.get_value())
# }}}
# do_move {{{
# moves operand 2 to operand 1 location.  In other words, it assign the value
# of operand 2 to operand 1 location.
def do_move(op1, op2):
  op1.set_value(op2.get_value())
# }}}
# do_branch {{{
# assigns the value of operand 1 to the Program Counter unconditionally.
# this function ignores operand 2 entirely.
def do_branch(op1=None, op2=None):
  # FIXED: check for address
  pc = Hardware.RAM[Hardware.PC]
  if pc < 0 or pc > Hardware.RAM_USER_LIMIT_MAX:
    raise InvalidMemoryAddress(pc)
  else:
    Hardware.PC = Hardware.RAM[Hardware.PC]
# }}}
# do_branch_on_negative {{{
# assigns the value of operand 1 to the Program Counter only if operand 1 is
# a negative number.
# this function ignores operand 2 entirely.
def do_branch_on_negative(op1, op2=None):
  if op1.get_value() < 0:
    pc = Hardware.RAM[Hardware.PC]
    if pc < 0 or pc > Hardware.RAM_USER_LIMIT_MAX:
      raise InvalidMemoryAddress(pc)
    else:
      Hardware.PC = Hardware.RAM[Hardware.PC]
  else:
    # skip the branch point
    Hardware.PC += 1
# }}}
# do_branch_on_positive {{{
# assigns the value of operand 1 to the Program Counter only if operand 1 is
# a positive number.
# this function ignores operand 2 entirely.
def do_branch_on_positive(op1, op2=None):
  if op1.get_value() > 0:
    pc = Hardware.RAM[Hardware.PC]
    if pc < 0 or pc > Hardware.RAM_USER_LIMIT_MAX:
      raise InvalidMemoryAddress(pc)
    else:
      Hardware.PC = Hardware.RAM[Hardware.PC]
  else:
    # skip the branch point
    Hardware.PC += 1
# }}}
# do_branch_on_zero {{{
# assigns the value of operand 1 to the Program Counter only if operand 1 is
# zero.
# this function ignores operand 2 entirely.
def do_branch_on_zero(op1, op2=None):
  if op1.get_value() == 0:
    pc = Hardware.RAM[Hardware.PC]
    if pc < 0 or pc > Hardware.RAM_USER_LIMIT_MAX:
      raise InvalidMemoryAddress(pc)
    else:
      Hardware.PC = Hardware.RAM[Hardware.PC]
  else:
    # skip the branch point
    Hardware.PC += 1
# }}}
# do_push {{{
# this function pushes the value of operand 1 onto the stack
# the stack pointer is one above the last value
# this function ignores operand 2 entirely
def do_push(op1, op2=None):
  # FIXED: Hardware.SP point to the _next_ position that a value is pushed.
  if Hardware.SP > RAM_STACK_LIMIT_MAX:
    raise StackOverflow()
  else:
    Hardware.RAM[Hardware.SP] = op1.get_value()
    Hardware.SP += 1
# }}}
# do_pop {{{
# this function pops the last value of the stack
# the stack pointer is one above the last value
# this function ignores operand 1 and 2 entirely
def do_pop(op1=None, op2=None):
  # FIXED: Hardware.SP point to the _next_ position that a value is pushed.
  if Hardware.SP <= RAM_STACK_LIMIT_MIN:
    raise StackUnderflow()
  else:
    Hardware.SP -= 1
    return Hardware.RAM[Hardware.SP]
# }}}
# do_system_call {{{
# this function ignores operand 2 entirely
def do_system_call(op1, pcb):
  Hardware.PSR = Globals.OSMode
  syscall = op1.get_value()

  if syscall == 1:
    # create process
    # input: gpr1 -> start address of new process
    # output: gpr0 -> return code
    # output: gpr2 -> PID
    pid = createChildProcess(Hardware.GPR[1], Globals.DefaultPriority)
    Hardware.GPR[0] = 0 # 0 is success
    Hardware.GPR[2] = pid

  elif syscall == 2:
    # delete process
    # input: gpr1 -> PID
    # output: none
    pid = Hardware.GPR[1]
    print("terminating child pid:%s" % pid)

    temp = queue.PriorityQueue()
    while not Globals.RQ.empty():
      t = Globals.RQ.get()
      if Hardware.RAM[t.pid] == pid:

        terminateProcess(t)
      else:
        temp.put(t)
    Globals.RQ = temp

  elif syscall == 3:
    # inquire about process, only self is implemented per project document
    # input: none
    # output: gpr0 -> status
    # output: gpr1 -> pid
    # output: gpr2 -> priority
    # output: gpr3 -> state
    # output: gpr4 -> reason for waiting
    Hardware.GPR[0] = 0 # 0 is success
    Hardware.GPR[1] = Hardware.RAM[pcb.pid]
    Hardware.GPR[2] = Hardware.RAM[pcb.priority]
    Hardware.GPR[3] = Hardware.RAM[pcb.state]
    Hardware.GPR[4] = Hardware.RAM[pcb.reasonForWaiting]

  elif syscall == 4:
    # allocate mem
    # input: gpr2 -> size
    # output: gpr0 -> return code: 0 -> failure, 1 -> success
    # output: gpr1 -> start address
    print("system call: allocate memory")
    print("PID: {}".format(Hardware.RAM[pcb.pid]))
    print("input: gpr2 = size")
    print("output: gpr0 = return code, gpr1 = start address")
    addr = allocateStack(Hardware.GPR[2])
    if addr > 0:
      Hardware.GPR[0] = 1
      Hardware.GPR[1] = addr
    else:
      Hardware.GPR[0] = 0
      Hardware.GPR[1] = 0

  elif syscall == 5:
    # free mem
    # input: gpr1 -> start
    # input: gpr2 -> size
    # output: gpr0 -> return code: 0 -> failure, 1 -> success
    print("system call: free memory")
    print("PID: {}".format(Hardware.RAM[pcb.pid]))
    print("input: gpr1 = start address, gpr2 = size")
    print("output: gpr0 = return code")
    start = Hardware.GPR[1]
    size = Hardware.GPR[2]

    if (start + size > Hardware.RAM_STACK_LIMIT_MAX or
        start < Hardware.RAM_STACK_LIMIT_MIN):
      Hardware.GPR[0] = 0
    else:
      freeStackMemory(start, size)
      Hardware.GPR[0] = 1

  elif syscall == 6:
    # send message to message queue
    # input: gpr1 -> received id
    # input: gpr2 -> start addr
    # output: gpr0 -> return code
    # return code: 0 -> success, 1 -> full queue, 2 -> no pid found
    rPid = Hardware.GPR[1]
    addr = Hardware.GPR[2]
    
    if rPid in Globals.MQ.keys():
      if len(Globals.MQ[rPid]) >= 10:
        Hardware.GPR[0] = 1
      else:
        Globals.MQ[rPid].insert(0, addr)
        Hardware.GPR[0] = 0
    else:
      Hardware.GPR[0] = 2

  elif syscall == 7:
    # receive message from message queue
    # input: gpr1 -> receiver pid
    # output: gpr0 -> status
    # output: gpr2 -> start addr
    rPid = Hardware.GPR[1]
    addr = Hardware.GPR[2]

    if rPid in Globals.MQ.keys():
      if len(Globals.MQ[rPid]) <= 0:
        print("message queue empty, suspending process")
        raise Suspend(-2)
      else:
        Hardware.GPR[0] = 0
        Hardware.GPR[2] = Globals.MQ[rPid].pop()
    else:
      print("no such queue, suspending process")
      raise Suspend(-2)

    m = Globals.MQ[rPid]

  elif syscall == 8:
    # get one character from keyboard
    # input: none
    # output: gpr0 -> return code
    # output: gpr1 -> ascii code of input
    c = input("enter character for pid %s: " % Hardware.RAM[pcb.pid])
    if c != "":
      Hardware.GPR[0] = 0
      Hardware.GPR[1] = ord(c)
    else:
      Hardware.GPR[0] = -1

  elif syscall == 9:
    # put one character on display
    # input: gpr1 -> ascii code of output
    # output: gpr0 -> return code
    Hardware.GPR[0] = 0
    print("print call: %s" % chr(Hardware.RAM[Hardware.GPR[1]]))

  elif syscall == 10:
    # get current clock time
    # input: none
    # output: gpr0 -> result status
    # output: gpr1 -> clock
    Hardware.GPR[1] = Hardware.CLOCK
    Hardware.GPR[0] = 0 # 0 is success

  elif syscall == 11:
    # set clock time
    # input: gpr1 -> clock
    # output: gpr0 -> result status
    Hardware.CLOCK = Hardware.GPR[1]
    Hardware.GPR[0] = 0 # 0 is success

  else:
    print("invalid system call")

  Hardware.PSR = Globals.UserMode
# }}}
# }}}
# list of supported instructions {{{
# this data structure contains the properties of all the instruction supported
# by the HYPO machine.  It is a map with the following structure:
#
# { opcode: properties }
#
# where opcode is the opcode of the instruction (an integer) and properties is
# another map with the following structure:
#
# { string: value }
#
# where string can be "name", "exec_time", or "exec".
#
# "name": is a human readable name for the opcode (string)
# "exec_time": is the clock cycle of the instruction (integer)
# "exec": the unified execution function of the instruction that handles
#         this instruction

instructions = {
  0:
    {"name": "halt"
    ,"exec_time": 0
    ,"exec": do_halt 
    },
  1:
    {"name": "add"
    ,"exec_time": 3
    ,"exec": do_add
    },
  2:
    {"name": "subtract"
    ,"exec_time": 3
    ,"exec": do_subtract
    },
  3:
    {"name": "multiply"
    ,"exec_time": 6
    ,"exec": do_multiply
    },
  4:
    {"name": "divide"
    ,"exec_time": 6
    ,"exec": do_divide
    },
  5:
    {"name": "move"
    ,"exec_time": 2
    ,"exec": do_move
    },
  6:
    {"name": "branch"
    ,"exec_time": 2
    ,"exec": do_branch
    },
  7:
    {"name": "branch_on_negative"
    ,"exec_time": 4
    ,"exec": do_branch_on_negative
    },
  8:
    {"name": "branch_on_positive"
    ,"exec_time": 4
    ,"exec": do_branch_on_positive
    },
  9:
    {"name": "branch_on_zero"
    ,"exec_time": 4
    ,"exec": do_branch_on_zero
    },
  10:
    {"name": "push"
    ,"exec_time": 2
    ,"exec": do_push
    },
  11:
    {"name": "pop"
    ,"exec_time": 2
    ,"exec": do_pop
    },
  12:
    {"name": "system_call"
    ,"exec_time": 12
    ,"exec": do_system_call
    }
  }
# }}}
# createProcess {{{
# creates a process based on input filename and priority
# input: filename:string, priority:integer
# output: none
def createProcess(filename, priority):
  print("creating process {} priority: {}".format(filename, priority))
  pcb = PCB(allocateOSMemory(PCB.size))
  initializePCB(pcb, priority)
  Hardware.RAM[pcb.PC] = absoluteLoader(filename)

  # store stack info in pcb
  # empty stack is low address, full stack is high address to be consistent
  # with other parts of the HYPO machine
  stackSize = 10
  stack = allocateStack(stackSize)
  Hardware.RAM[pcb.stackStart] = stack
  Hardware.RAM[pcb.SP] = stack
  Hardware.RAM[pcb.stackSize] = stackSize

  Globals.MQ[Hardware.RAM[pcb.pid]] = [] # 10 max

  # dump program area
  dumpMemory("created process {}".format(pcb.start), Hardware.RAM[pcb.PC], 40)

  # print pcb
  printPCB(pcb)

  Globals.RQ.put(pcb)
# }}}
# createChildProcess {{{
# creates a child process in response to system call 1.
# input: start location, priority
# output: none
def createChildProcess(location, priority):
  print("creating child process {} priority: {}".format(location, priority))
  pcb = PCB(allocateOSMemory(PCB.size))
  initializePCB(pcb, priority)
  Hardware.RAM[pcb.PC] = location

  # store stack info in pcb
  # empty stack is low address, full stack is high address to be consistent
  # with other parts of the HYPO machine
  stackSize = 10
  stack = allocateStack(stackSize)
  Hardware.RAM[pcb.stackStart] = stack
  Hardware.RAM[pcb.SP] = stack
  Hardware.RAM[pcb.stackSize] = stackSize

  Globals.MQ[Hardware.RAM[pcb.pid]] = [] # 10 max

  # dump program area
  dumpMemory("created process {}".format(pcb.start), Hardware.RAM[pcb.PC], 40)

  # print pcb
  printPCB(pcb)

  Globals.RQ.put(pcb)

  return Hardware.RAM[pcb.pid]
# }}}
# initializePCB {{{
# initializes a given PCB based on the current state of the system.  This function
# also assigns a free block of memory to the PCB.
# input: pcb:PCB, priority:integer
# output: none
def initializePCB(pcb, priority):
  for i in range(pcb.start, pcb.start + pcb.size):
    Hardware.RAM[i] = 0

  Globals.PID += 1
  Hardware.RAM[pcb.pid] = Globals.PID
  Hardware.RAM[pcb.priority] = priority
  Hardware.RAM[pcb.state] = Globals.ReadyState
  Hardware.RAM[pcb.nextPCB] = Globals.EndOfList
# }}}
# insertIntoRQ {{{
# inserts a pcb into the scheduling priority queue
# input: pcb:PCB
# output: none
def insertIntoRQ(pcb):
  Hardware.RAM[pcb.state] = Globals.ReadyState
  Hardware.RAM[pcb.nextPCB] = Globals.EndOfList

  # RQ is a priority queue
  Globals.RQ.put(pcb)
# }}}
# selectReadyProcess {{{
# returns the first process from the scheduling priority queue
# input: none
# output: a PCB object
def selectReadyProcess():
  pcb = Globals.RQ.get()
  print("selecting %s" % pcb.start)
  return pcb
# }}}
# saveContext {{{
# saves the current context of the hardware in the current process's PCB
# input: pcb:PCB
# output: none
def saveContext(pcb):
  Hardware.RAM[pcb.gpr0] = Hardware.GPR[0]
  Hardware.RAM[pcb.gpr1] = Hardware.GPR[1]
  Hardware.RAM[pcb.gpr2] = Hardware.GPR[2]
  Hardware.RAM[pcb.gpr3] = Hardware.GPR[3]
  Hardware.RAM[pcb.gpr4] = Hardware.GPR[4]
  Hardware.RAM[pcb.gpr5] = Hardware.GPR[5]
  Hardware.RAM[pcb.gpr6] = Hardware.GPR[6]
  Hardware.RAM[pcb.gpr7] = Hardware.GPR[7]
  Hardware.RAM[pcb.SP] = Hardware.SP
  Hardware.RAM[pcb.PC] = Hardware.PC
# }}}
# dispatch {{{
# restores the context of a given process using its PCB
# input: pcb:PCB
# output: none
def dispatch(pcb):
  Hardware.GPR[0] = Hardware.RAM[pcb.gpr0]
  Hardware.GPR[1] = Hardware.RAM[pcb.gpr1]
  Hardware.GPR[2] = Hardware.RAM[pcb.gpr2]
  Hardware.GPR[3] = Hardware.RAM[pcb.gpr3]
  Hardware.GPR[4] = Hardware.RAM[pcb.gpr4]
  Hardware.GPR[5] = Hardware.RAM[pcb.gpr5]
  Hardware.GPR[6] = Hardware.RAM[pcb.gpr6]
  Hardware.GPR[7] = Hardware.RAM[pcb.gpr7]
  Hardware.SP = Hardware.RAM[pcb.SP]
  Hardware.PC = Hardware.RAM[pcb.PC]
# }}}
# terminateProcess {{{
# terminates a given process
# input: pcb:PCB
# output: none
def terminateProcess(pcb):
  print("terminating %s" % pcb.start)
  freeStackMemory(Hardware.RAM[pcb.stackStart], Hardware.RAM[pcb.stackSize])
  freeOSMemory(pcb.start, PCB.size)

  try:
    del Globals.MQ[Hardware.RAM[pcb.pid]]
  except KeyError as e:
    # in case no message was passed around
    pass
# }}}
# allocateStack {{{
# allocates a block of memory from the user memory.
# input: size:integer
# output: a MemoryBlock object
def allocateStack(size):
  if size < 0:
    print("Error -100: invalid size")
    return -1

  if size < 2:
    # minimum size of 2
    size = 2

  for i in range(len(Globals.userFreeList)):

    if Globals.userFreeList[i].size == size:
      # return the whole block
      addr = Globals.userFreeList[i].start
      del Globals.userFreeList[i]
      return addr

    elif Globals.userFreeList[i].size > size:
      # chop off the block to appropriate size
      addr = Globals.userFreeList[i].start
      Globals.userFreeList[i].start = Globals.userFreeList[i].start + size
      return addr

  # no memory left?
  print("not enough memory")
  return -1
# }}}
# freeStackMemory {{{
# frees a block of memory from user list given its start position and size
# input: start:integer, size:integer
# output: none
def freeStackMemory(start, size):
  if (start < Hardware.RAM_STACK_LIMIT_MIN or
     start+size > Hardware.RAM_STACK_LIMIT_MAX):
    raise InvalidMemoryAddress(start)
  else:
    Globals.userFreeList.insert(0, MemoryBlock(start, size))
# }}}
# allocateOSMemory {{{
# allocates a block of memory from the OS memory.
# input: size:integer
# output: a MemoryBlock object
def allocateOSMemory(size):
  if size < 0:
    print("Error -100: invalid size")
    return -1

  if size < 2:
    # minimum size of 2
    size = 2

  for i in range(len(Globals.osFreeList)):

    if Globals.osFreeList[i].size == size:
      # return the whole block
      addr = Globals.osFreeList[i].start
      del Globals.osFreeList[i]
      return addr

    elif Globals.osFreeList[i].size > size:
      # chop off the block to appropriate size
      addr = Globals.osFreeList[i].start
      Globals.osFreeList[i].start = Globals.osFreeList[i].start + size
      return addr

  # no memory left?
  print("not enough memory")
  return 0
# }}}
# freeOSMemory {{{
# frees a block of memory from OS list given its start position and size
# input: start:integer, size:integer
# output: none
def freeOSMemory(start, size):
  if (start < Hardware.RAM_OS_LIMIT_MIN or
     start+size > Hardware.RAM_OS_LIMIT_MAX):
    raise InvalidMemoryAddress(start)
  else:
    Globals.osFreeList.insert(0, MemoryBlock(start, size))
# }}}
# handleInterrupts {{{
# this function handles software interrupts by asking the user what he wants
# to do
# input: none
# output: none
def handleInterrupts():
  i = ""
  while i == "":
    i = input("interrupt [0-4]? ")

  try:
    i = int(i)

    if i == 0:
      # no interrupt
      pass
    elif i == 1:
      # create process
      filename = input("filename? ")
      createProcess(filename, Globals.DefaultPriority)
    elif i == 2:
      # shutdown
      raise Shutdown()
    elif i == 3:
      # input operation
      pid = int(input("pid of process? "))
      c = sys.stdin.read(1)

      for proc in Globals.WQ:
        if Hardware.RAM[proc.pid] == pid:
          Hardware.RAM[proc.state] = Globals.ReadyState
          Hardware.RAM[proc.gpr0] = 0 # 0 for success
          Hardware.RAM[proc.gpr1] = ord(c)
          Globals.RQ.put(proc)

      Globals.WQ = filter(lambda x: Hardware.RAM[x.pid] != pid, Globals.WQ)

    elif i == 4:
      # output operation
      pid = int(input("pid of process? "))

      for proc in Globals.WQ:
        if Hardware.RAM[proc.pid] == pid:
          print(chr(Hardware.RAM[proc.gpr1]))
          Globals.RQ.put(proc)

      Globals.WQ = filter(lambda x: Hardware.RAM[x.pid] != pid, Globals.WQ)

    else:
      print("invalid interrupt")
  except ValueError as e:
    print("invalid input")
# }}}
# initializeSystem {{{
# initializes the hardware components of the system (global variables) mostly
# to zero.
# 
# input: none
# output: none
def initializeSystem():
  print("initializing system")
  # the system (global variables) are already initialized, but we will
  # initialize them here again to be sure
  Hardware.MAR = 0 
  Hardware.MBR = 0 
  Hardware.IR  = 0 
  Hardware.SP  = Hardware.RAM_STACK_LIMIT_MIN
  Hardware.PC  = 0 
  Hardware.PSR = 0 
  Hardware.CLOCK = 0
  Hardware.GPR = [0] * 8
  Hardware.RAM = [0] * Hardware.RAM_LIMIT

  # initializing user space free list
  Hardware.RAM[4000] = -1
  Hardware.RAM[4001] = 2000

  Globals.osFreeList = [MemoryBlock(Hardware.RAM_OS_LIMIT_MIN,
                                    Hardware.RAM_OS_LIMIT_MAX
                                    - Hardware.RAM_OS_LIMIT_MIN)]
  Globals.userFreeList = [MemoryBlock(Hardware.RAM_STACK_LIMIT_MIN,
                                      Hardware.RAM_STACK_LIMIT_MAX
                                      - Hardware.RAM_STACK_LIMIT_MIN)]

  # 256 is lowest priority, 0 is highest
  createProcess("null.hypo", 256)
# }}}
# absoluteLoader {{{
# loads the content of an executable file into memory to be executed later
#
# input: the filename of the executable file (string)
# output: PC to be used for executing this program
def absoluteLoader(filename):
  print("Loading executable '{0}'".format(filename))

  # this might throw IOError exception, caught in main
  with open(filename, "r") as f:
    for line in f:
      try:
        addr = int(line.split()[0])
        val  = int(line.split()[1])
      except (IndexError, ValueError) as e:
        raise SynError(line.strip())

      if addr >= 0 and addr <= 9999:
        Hardware.RAM[addr] = val
      elif addr == Hardware.END_ADDRESS:
        print("end of program reached")
        return val
      else:
        raise InvalidAddress(addr, val)
    # in case no END_ADDRESS was detected
    raise EOFReached()
# }}}
# executeProgram {{{
# This function executes one instruction at a time pointed by program counter
# by performing (a) fetch instruction cycle, (b) decode instruction cycle,
# and (c) execute instruction cycle
#
# This function executes one instruction at a time pointed by program counter 
# by performing (a) fetch instruction cycle, (b) decode instruction cycle, 
# and (c) execute instruction cycle.
#
# input: none
# output: execution status of the program
def executeProgram(pcb):
  print("executing program")
  try:
    t = 0
    while True:

      # fetch
      ins_code = Hardware.RAM[Hardware.PC]
      oldPC = Hardware.PC

      Hardware.PC += 1

      # decode
      # instruction:
      # opcode (1 or 2 digits) | op1 mode | op1 gpr | op2 mode | op2 gpr
      #
      # convert the instruction to string, cut it to appropriate size, then
      # convert it back to int
      opcode   = ins_code // 10000
      rem      = ins_code % 10000

      op1_mode = rem // 1000
      rem      = rem % 1000
      op1_gpr  = rem // 100
      rem      = rem % 100
      op1 = validate_operand(Operand(op1_mode, op1_gpr))

      op2_mode = rem // 10
      rem      = rem % 10
      op2_gpr  = rem
      op2_addr = 0
      op2 = validate_operand(Operand(op2_mode, op2_gpr))

      # execute

      #print("executing statement {}: {}".format(
        #oldPC, instructions[opcode]["name"]))

      try:
        if instructions[opcode]["name"] == "system_call":
          instructions[opcode]["exec"](op1, pcb)
        else:
          instructions[opcode]["exec"](op1, op2)
      except Suspend as e:
        Hardware.PC += e.pcAdjustment
        t = t + Globals.TimeSlice + 1

      # FIXED: CLOCK incremented after successful execution
      Hardware.CLOCK += instructions[opcode]["exec_time"]

      t += instructions[opcode]["exec_time"]

      #dumpMemory("variable area: ", 4, 6)
      #dumpMemory("2030 area: ", 2030, 10)

      if t >= Globals.TimeSlice:
        raise TimeSliceExpired()
  except KeyError as e:
    raise InvalidOpcode(opcode)
  else:
    if Hardware.PC < 0:
      raise InvalidAddress(Hardware.PC)
    else:
      raise RestrictedMemoryAccess(Hardware.PC, "User")
# }}}
# dumpMemory {{{
# displays the content of the Hypo machine GPRs, the Clock, content of given 
# memory locations, and the given string.
#
# The format of display is shown below when the function is called as follows 
# for displaying values of memory locations 10 to 35.
#
# Memory dump after loading program
# GPRs: 	G0    G1    G2    G3    G4    G5    G6    G7    PC    SP 
# Address	+0    +1    +2    +3    +4    +5    +6    +7    +8    +9
# 10		  v10   v11   v12   v13   v14   v15   v16   v17   v18   v19
# 20      v20   v21   v22   v23   v24   v25   v26   v27   v28   v29
# 30      v30   v31   v32   v33   v34   v35
# Clock = value
#
# input:
#   character string to be output (string)
#   starting address of memory (int)
#   size of memory to be output(int)
# 
# output:
#   error status (int)
def dumpMemory(prompt, start, size):
  print(prompt)

  if start < 0 or (start + size) > Hardware.RAM_LIMIT:
    print("illegal dump start:{} size:{}".format(start, size))
    return

  if size <= 0:
    print("not allocated")
    return

  # print GPRs
  print("GPRs:\t", end="")
  for i in Hardware.GPR:
    print("{0}".format(str(i) + "\t"), end="")
  print()

  # print Address values
  print("Addr:\t+0\t+1\t+2\t+3\t+4\t+5\t+6\t+7\t+8\t+9", end="")
  for i in range(start, start+size):
    if (i - start) % 10 == 0:
      print("\n{:0>4}\t".format(i), end="")
    print("{0}\t".format(Hardware.RAM[i] if Hardware.RAM[i] > 0 else '0000'), end="")
  print()


  # print clock value
  print("Clock = {0}".format(Hardware.CLOCK))

# }}}
# dumpAllocatedMemory {{{
# same format as dumpMemory.  Dumps a process's stack and allocated user memory
# input: pcb:PCB
# output: none
def dumpCurrentAllocatedMemory(pcb):
    dumpMemory("Allocated stack memory"
              , Hardware.RAM[pcb.stackStart]
              , Hardware.RAM[pcb.stackSize])
    #dumpMemory("Allocated dynamic user memory"
              #, Hardware.GPR[1]
              #, Hardware.GPR[2])
    dumpMemory("Allocated message", Hardware.RAM[13], 10)
# }}}
# dumpRQ {{{
# prints the contents of the Ready Queue in a simple format
# input: none
# output: none
def dumpRQ():
  print("Ready Queue dump")
  tempQ = queue.PriorityQueue()

  while not Globals.RQ.empty():
    pcb = Globals.RQ.get()
    print("start: {}, size: {}".format(pcb.start, PCB.size))
    tempQ.put(pcb)

  while not tempQ.empty():
    pcb = tempQ.get()
    Globals.RQ.put(pcb)
# }}}
# dumpWQ {{{
# prints the contents of the Waiting Queue in a simple format
# input: none
# output: none
def dumpWQ():
  print("Waiting Queue dump")
  if len(Globals.WQ) <= 0:
    print("none found")
    return

  for i in Globals.WQ:
    pcb = Globals.WQ[i]
    print("start: {}, size: {}".format(pcb.start, PCB.size))
# }}}
# printPCB {{{
# prints the contents of a PCB with the same format as dumpMemory
# input: pcb:PCB
# output: none
def printPCB(pcb):
  dumpMemory("PCB starting at %d:" % pcb.start, pcb.start, PCB.size)
# }}}
# main {{{
# This function initializes the system and runs it.
# input: none
# output: none
def main():
  try:
    initializeSystem()

    while True:
      handleInterrupts()

      print("Other processes")
      dumpRQ()
      dumpWQ()

      pcb = selectReadyProcess()
      dispatch(pcb)

      try:
        executeProgram(pcb)

      except TimeSliceExpired:
        dumpCurrentAllocatedMemory(pcb)
        saveContext(pcb)
        insertIntoRQ(pcb)

      except StartOfInput:
        Hardware.RAM[pcb.state] = Globals.WaitingState
        Hardware.RAM[pcb.reasonForWaiting] = Globals.ReasonInput
        Globals.WQ.append(pcb)

      except StartOfOutput:
        Hardware.RAM[pcb.state] = Globals.WaitingState
        Hardware.RAM[pcb.reasonForWaiting] = Globals.ReasonOutput
        Globals.WQ.append(pcb)

      except WaitingForMessage:
        # waiting for message is already handled in system_call
        pass

      except Halt:
        print("halting process")
        dumpCurrentAllocatedMemory(pcb)
        terminateProcess(pcb)

        if Globals.RQ.empty():
          createProcess("null.hypo", 256)

  except Shutdown:
    print("shutting down system...")
    while not Globals.RQ.empty():
      terminateProcess(Globals.RQ.get())
    return 0

  except IOError as e:
    print("Error [{0}] occurred while loading '{1}': {2}".format(
      e.errno, filename, e.strerror))
    return -1

  except (EOFReached, InvalidAddress,
      MemoryOverflow, InvalidOpcode,
      InvalidOperandMode, InvalidMemoryAddress,
      InvalidGPRAddress, RestrictedMemoryAccess,
      StackOverflow, StackUnderflow, SynError) as e:
    print(e.strerror)
    return e.errno

  except ZeroDivisionError as e:
    print("Error [-50] Division by Zero detected")
    return -50

  else:
    return 0

# }}}

if __name__ == "__main__":
  status = main()
  sys.exit(status)

