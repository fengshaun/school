/* 
 * server FTP program
 *
 * This program is used transfer files to and from a client
 *
 * use: COMMAND [OPTION] [PATH]
 *
 * user username      authenticates username
 * pass password      authenticates user with password
 * quit               quits ftp
 * mkdir directory    creates directory
 * rmdir directory    removes directory
 * cd [directory]     changes current working directory
 * dele [file]        removes file
 * pwd                prints current directory
 * ls                 prints the contents of current directory
 * stat               print status
 * help               prints this message
 *
 */

#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <netdb.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>

#define SERVER_FTP_PORT 4000
#define DATA_CONNECTION_PORT 4001


/* Error and OK codes */
#define OK 0
#define ER_INVALID_HOST_NAME -1
#define ER_CREATE_SOCKET_FAILED -2
#define ER_BIND_FAILED -3
#define ER_CONNECT_FAILED -4
#define ER_SEND_FAILED -5
#define ER_RECEIVE_FAILED -6
#define ER_ACCEPT_FAILED -7


/* Function prototypes */

int svcInitServer(int *s);
int clntConnect(char  *serverName, int *s);
int sendMessage (int s, char *msg, int  msgSize);
int receiveMessage(int s, char *buffer, int  bufferSize, int *msgSize, int p);


/* List of all global variables */

char userCmd[1024]; /* user typed ftp command line received from client */
char *cmd;    /* ftp command (without argument) extracted from userCmd */
char *argument; /* argument (without ftp command) extracted from userCmd */
char replyMsg[1024];       /* buffer to send reply message to client */

// assuming a global user list
char allUser[3][20] = {"foo", "bar", "baz"};
const int allUserLength = 3;
char allPass[3][20] = {"foo12", "bar12", "baz12"};
int userPos = -1;
int loggedIn = 0;


/*
 * main
 *
 * Function to listen for connection request from client
 * Receive ftp command one at a time from client
 * Process received command
 * Send a reply message to the client after processing the command with staus of
 * performing (completing) the command
 * On receiving QUIT ftp command, send reply to client and then close all sockets
 *
 * Parameters
 * argc   - Count of number of arguments passed to main (input)
 * argv   - Array of pointer to input parameters to main (input)
 *       It is not required to pass any parameter to main
 *       Can use it if needed.
 *
 * Return status
 *  0     - Successful execution until QUIT command from client 
 *  ER_ACCEPT_FAILED  - Accepting client connection request failed
 *  N     - Failed stauts, value of N depends on the command processed
 */

int main( 
    int argc,
    char *argv[]
    )
{
  /* List of local varibale */

  int msgSize;        /* Size of msg received in octets (bytes) */
  int listenSocket;   /* listening server ftp socket for client connect request */
  int ccSocket;        /* Control connection socket - to be used in all client communication */
  int status;


  /*
   * NOTE: without \n at the end of format string in printf,
   * UNIX will buffer (not flush)
   * output to display and you will not see it on monitor.
   */
  printf("Started execution of server ftp\n");


  /*initialize server ftp*/
  printf("Initialize ftp server\n");  /* changed text */

  status=svcInitServer(&listenSocket);
  if(status != 0)
  {
    printf("Exiting server ftp due to svcInitServer returned error\n");
    exit(status);
  }


  printf("ftp server is waiting to accept connection\n");

  /* wait until connection request comes from client ftp */
  ccSocket = accept(listenSocket, NULL, NULL);

  printf("Came out of accept() function \n");

  if(ccSocket < 0)
  {
    perror("cannot accept connection:");
    printf("Server ftp is terminating after closing listen socket.\n");
    close(listenSocket);  /* close listen socket */
    return (ER_ACCEPT_FAILED);  // error exist
  }

  printf("Connected to client, calling receiveMsg to get ftp cmd from client \n");


  /* Receive and process ftp commands from client until quit command.
   * On receiving quit command, send reply to client and 
   * then close the control connection socket "ccSocket". 
   */

  do
  {
    /* Receive client ftp commands until */
    status=receiveMessage(ccSocket, userCmd, sizeof(userCmd), &msgSize, 1);
    if(status < 0)
    {
      printf("Receive message failed. Closing control connection \n");
      printf("Server ftp is terminating.\n");
      break;
    }


    /* Separate command and argument from userCmd */
    cmd = strtok(userCmd, " ");
    argument = strtok(NULL, " ");

    /* from here on until the end of if-else cascade,
     * we just essentially do something based on the
     * command received from the client
     */

    if (strcmp(cmd, "user") == 0) {
      // finding what user is trying to login
      // the list of users is at the top of this
      // file
      int foundPos = -1;
      int i;
      for (i = 0; i < allUserLength; i++) {
        if (strcmp(allUser[i], argument) == 0) {
          foundPos = i;
        }
      }

      if (foundPos < 0) {
        strcpy(replyMsg, "501 Username not okay, login failed");
        userPos = -1;
      } else {
        strcpy(replyMsg, "331 Username okay, need password");
        userPos = foundPos;
      }
    } else if (strcmp(cmd, "pass") == 0) {
      // now just match the username with password
      if ((userPos >= 0) && (strcmp(argument, allPass[userPos]) == 0)) {
        strcpy(replyMsg, "230 User logged in, proceed.");
        loggedIn = 1;
      } else {
        strcpy(replyMsg, "501 Password not okay, login failed");
        loggedIn = 0;
      }
    } else if (strcmp(cmd, "quit") == 0) {
      // do nothing, let it roll!
      // it will quit on its own
    } else if (strcmp(cmd, "mkdir") == 0) {
      /* the FTP spec says we have to reply with the full path
       * of the new directory, so this fragment is constructing 
       * the full path.  mkdir works better with the full path 
       * anyway
       */
      char fullPath[2048];
      if (argument[0] == '/') {
        strcpy(fullPath, argument);
      } else {
        strcpy(fullPath, getcwd(NULL, 0));
        strcat(fullPath, "/");
        strcat(fullPath, argument);
      }

      char fullCmd[1024];
      sprintf(fullCmd, "mkdir %s", fullPath);

      // for most of the following commands, we just send the unix equivalent
      // to the shell and only get the return code if there is an error
      if (system(fullCmd) == 0) {
        // this is why fullPath was constructed
        sprintf(replyMsg, "257 \"%s\" directory created", fullPath);
      } else {
        strcpy(replyMsg, "501 mkdir command failed: ");
        strcat(replyMsg, strerror(errno));
      }
    } else if (strcmp(cmd, "rmdir") == 0) {
      // same deal as mkdir, just removing instead of creating
      char fullPath[2048];
      if (argument[0] == '/') {
        strcpy(fullPath, argument);
      } else {
        strcpy(fullPath, getcwd(NULL, 0));
        strcat(fullPath, "/");
        strcat(fullPath, argument);
      }

      char fullCmd[1024];
      sprintf(fullCmd, "rmdir %s", fullPath);

      if (system(fullCmd) == 0) {
        sprintf(replyMsg, "250 \"%s\" directory removed", fullPath);
      } else {
        strcpy(replyMsg, "501 rmdir command failed: ");
        strcat(replyMsg, strerror(errno));
      }
    } else if (strcmp(cmd, "cd") == 0) {
      // chdir requires full path
      // otherwise, same deal as mkdir and rmdir in terms
      // of command execution
      char fullPath[2048];
      if (argument[0] == '/') {
        chdir(argument);
      } else {
        strcpy(fullPath, getcwd(NULL, 0));
        strcat(fullPath, "/");
        strcat(fullPath, argument);
      }

      if (chdir(argument) == 0) {
        sprintf(replyMsg, "250 Directory changed to %s", getcwd(NULL, 0));
      } else {
        strcpy(replyMsg, "501 change directory failed: ");
        strcat(replyMsg, strerror(errno));
      }
    } else if (strcmp(cmd, "dele") == 0) {
      // this is already a system call, not much to do
      if (remove(argument) == 0) {
        sprintf(replyMsg, "250 \"%s\" removed", argument);
      } else {
        strcpy(replyMsg, "501 dele command failed: ");
        strcat(replyMsg, strerror(errno));
      }
    } else if (strcmp(cmd, "pwd") == 0) {
      // again, a system call already
      char path[1024];
      getcwd(path, sizeof(path));

      sprintf(replyMsg, "212 Directory \"%s\"", path);
    } else if (strcmp(cmd, "ls") == 0) {
      /* we need to get the output of a command.  A file could be used,
       * or we can just pipe the output here!
       *
       * working with pipes and files is similar (both are streams)
       */
      FILE *f = popen("ls", "r");

      strcpy(replyMsg, "");
      char buf[2048];

      // read from the pipe and add it to a buffer to be sent
      // normally, this is done over a data connection, but 
      // we do it over the control connection for simplicity
      while (fgets(buf, sizeof(buf), f) != NULL) {
        strcat(replyMsg, buf);
      }

      strcat(replyMsg, "212 Directory listing");
      pclose(f);
    } else if (strcmp(cmd, "stat") == 0) {
      /* Just some status information.
       * Not the full deal, but only a representation
       * for the purposes of this assignment
       */
      strcpy(replyMsg, "Connection okay\n");
      strcat(replyMsg, "File transfer mode is ASCII\n");
      strcat(replyMsg, "211 STAT report");
    } else if (strcmp(cmd, "help") == 0) {
      strcpy(replyMsg, "use: COMMAND [OPTION] [PATH]\n");
      strcat(replyMsg, "\n");
      strcat(replyMsg, "user username  \tauthenticates username\n");
      strcat(replyMsg, "pass password  \tauthenticates user with password\n");
      strcat(replyMsg, "quit           \tquits ftp\n");
      strcat(replyMsg, "mkdir directory\tcreates directory\n");
      strcat(replyMsg, "rmdir directory\tremoves directory\n");
      strcat(replyMsg, "cd [directory] \tchanges current working directory\n");
      strcat(replyMsg, "dele [file]    \tremoves file\n");
      strcat(replyMsg, "pwd            \tprints current directory\n");
      strcat(replyMsg, "ls             \tprints the contents of current directory\n");
      strcat(replyMsg, "stat           \tprint status\n");
      strcat(replyMsg, "help           \tprints this message\n");
      strcat(replyMsg, "214 FTP help");
    } else if (strcmp(cmd, "put") == 0) {
      // "put" really means get when we are talking about the server
      int sock;
      clntConnect("127.0.0.1", &sock);

      // the filename in this case is the same as what is 
      // on the client side
      FILE *file = fopen(argument, "w");

      if (file == NULL) {
        strcpy(replyMsg, "450 Error creating file: ");
        strcat(replyMsg, strerror(errno));
      } else {
        int total = 0;
        int msgSize;
        char buffer[100];

        while(1) {
          /* essentially the same code as "get" on client side
           * with only a few modifications
           *
           * get block -> write block
           * rinse and repeat until EOF or error
           * lots of error checking here
           */
          int status = receiveMessage(sock, buffer, sizeof(buffer), &msgSize, 1);
          if (status != 0) {
            strcpy(replyMsg, "426 Error receiving file: ");
            strcat(replyMsg, strerror(errno));
          }
          if (msgSize <= 0) {
            break;
          }
          fwrite(buffer, 1, msgSize, file);
        } 

        if (total > 0) {
          strcpy(replyMsg, "250 file sent successfully");
        } else {
          strcpy(replyMsg, "426 Error receiving file: no data transmitted");
        }

        fclose(file);
      }

      close(sock);
    } else if (strcmp(cmd, "get") == 0) {
      int sock;
      clntConnect("127.0.0.1", &sock);

      FILE *file = fopen(argument, "r");
      char buffer[100];

      // some error checking
      if (file == NULL) {
        strcpy(replyMsg, "550 Error reading file: ");
        strcat(replyMsg, strerror(errno));
      } else {
        int status;
        while (!feof(file)) {
          // essentially the same code as "put" in client side
          // read block -> send block
          // rinse and repeat until error or EOF
          unsigned int rs = fread(buffer, 1, sizeof(buffer), file);
          status = sendMessage(sock, buffer, rs);
          if (status != 0) {
            strcpy(replyMsg, "426 Error sending file: ");
            strcat(replyMsg, strerror(errno));
            break;
          }
        }

        if (status == 0) {
          strcpy(replyMsg, "250 file received successfully");
        }

        fclose(file);
      }

      close(sock);
    } else {
      // the catch-all safety net
      strcpy(replyMsg, "500 Syntax error, command unrecognized");
    }

    /*
     * ftp server sends only one reply message to the client for 
     * each command received in this implementation.
     */
    status=sendMessage(ccSocket,replyMsg,strlen(replyMsg) + 1); /* Added 1 to include NULL character in */
    /* the reply string strlen does not count NULL character */
    if(status < 0)
    {
      break;  /* exit while loop */
    }
  }
  while(strcmp(cmd, "quit") != 0);

  printf("Closing control connection socket.\n");
  close(ccSocket);  /* Close client control connection socket */

  printf("Closing listen socket.\n");
  close(listenSocket);  /*close listen socket */

  printf("Existing from server ftp main \n");

  return (status);
}


/*
 * svcInitServer
 *
 * Function to create a socket and to listen for connection request from client
 *    using the created listen socket.
 *
 * Parameters
 * s    - Socket to listen for connection request (output)
 *
 * Return status
 *  OK      - Successfully created listen socket and listening
 *  ER_CREATE_SOCKET_FAILED - socket creation failed
 */

int svcInitServer (
    int *s    /*Listen socket number returned from this function */
    )
{
  int sock;
  struct sockaddr_in svcAddr;
  int qlen;

  /*create a socket endpoint */
  if( (sock=socket(AF_INET, SOCK_STREAM,0)) <0)
  {
    perror("cannot create socket");
    return(ER_CREATE_SOCKET_FAILED);
  }

  /*initialize memory of svcAddr structure to zero. */
  memset((char *)&svcAddr,0, sizeof(svcAddr));

  /* initialize svcAddr to have server IP address and server listen port#. */
  svcAddr.sin_family = AF_INET;
  svcAddr.sin_addr.s_addr=htonl(INADDR_ANY);  /* server IP address */
  svcAddr.sin_port=htons(SERVER_FTP_PORT);    /* server listen port # */

  /* bind (associate) the listen socket number with server IP and port#.
   * bind is a socket interface function 
   */
  if(bind(sock,(struct sockaddr *)&svcAddr,sizeof(svcAddr))<0)
  {
    perror("cannot bind");
    close(sock);
    return(ER_BIND_FAILED); /* bind failed */
  }

  /* 
   * Set listen queue length to 1 outstanding connection request.
   * This allows 1 outstanding connect request from client to wait
   * while processing current connection request, which takes time.
   * It prevents connection request to fail and client to think server is down
   * when in fact server is running and busy processing connection request.
   */
  qlen=1; 


  /* 
   * Listen for connection request to come from client ftp.
   * This is a non-blocking socket interface function call, 
   * meaning, server ftp execution does not block by the 'listen' funcgtion call.
   * Call returns right away so that server can do whatever it wants.
   * The TCP transport layer will continuously listen for request and
   * accept it on behalf of server ftp when the connection requests comes.
   */

  listen(sock,qlen);  /* socket interface function call */

  /* Store listen socket number to be returned in output parameter 's' */
  *s=sock;

  return(OK); /*successful return */
}

/*
 * clntConnect
 *
 * Function to create a socket, bind local client IP address and port to the socket
 * and connect to the server
 *
 * Parameters
 * serverName - IP address of server in dot notation (input)
 * s    - Control connection socket number (output)
 *
 * Return status
 *  OK      - Successfully connected to the server
 *  ER_INVALID_HOST_NAME  - Invalid server name
 *  ER_CREATE_SOCKET_FAILED - Cannot create socket
 *  ER_BIND_FAILED    - bind failed
 *  ER_CONNECT_FAILED - connect failed
 */

int clntConnect (
    char *serverName, /* server IP address in dot notation (input) */
    int *s      /* control connection socket number (output) */
    )
{
  int sock; /* local variable to keep socket number */

  struct sockaddr_in clientAddress;   /* local client IP address */
  struct sockaddr_in serverAddress; /* server IP address */
  struct hostent     *serverIPstructure;  /* host entry having server IP address in binary */


  /* Get IP address os server in binary from server name (IP in dot natation) */
  if((serverIPstructure = gethostbyname(serverName)) == NULL)
  {
    printf("%s is unknown server. \n", serverName);
    return (ER_INVALID_HOST_NAME);  /* error return */
  }

  /* Create control connection socket */
  if((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    perror("cannot create socket ");
    return (ER_CREATE_SOCKET_FAILED); /* error return */
  }

  /* initialize client address structure memory to zero */
  memset((char *) &clientAddress, 0, sizeof(clientAddress));

  /* Set local client IP address, and port in the address structure */
  clientAddress.sin_family = AF_INET; /* Internet protocol family */
  clientAddress.sin_addr.s_addr = htonl(INADDR_ANY);  /* INADDR_ANY is 0, which means */
  /* let the system fill client IP address */
  clientAddress.sin_port = 0;  /* With port set to 0, system will allocate a free port */
  /* from 1024 to (64K -1) */

  /* Associate the socket with local client IP address and port */
  if(bind(sock,(struct sockaddr *)&clientAddress,sizeof(clientAddress))<0)
  {
    perror("cannot bind");
    close(sock);
    return(ER_BIND_FAILED); /* bind failed */
  }


  /* Initialize serverAddress memory to 0 */
  memset((char *) &serverAddress, 0, sizeof(serverAddress));

  /* Set ftp server ftp address in serverAddress */
  serverAddress.sin_family = AF_INET;
  memcpy((char *) &serverAddress.sin_addr, serverIPstructure->h_addr, 
      serverIPstructure->h_length);
  serverAddress.sin_port = htons(DATA_CONNECTION_PORT);

  /* Connect to the server */
  if (connect(sock, (struct sockaddr *) &serverAddress, sizeof(serverAddress)) < 0)
  {
    perror("Cannot connect to server ");
    close (sock);   /* close the control connection socket */
    return(ER_CONNECT_FAILED);    /* error return */
  }


  /* Store listen socket number to be returned in output parameter 's' */
  *s=sock;

  return(OK); /* successful return */
}  // end of clntConnect() */

/*
 * sendMessage
 *
 * Function to send specified number of octet (bytes) to client ftp
 *
 * Parameters
 * s    - Socket to be used to send msg to client (input)
 * msg    - Pointer to character arrary containing msg to be sent (input)
 * msgSize  - Number of bytes, including NULL, in the msg to be sent to client (input)
 *
 * Return status
 *  OK    - Msg successfully sent
 *  ER_SEND_FAILED  - Sending msg failed
 */

int sendMessage(
    int    s, /* socket to be used to send msg to client */
    char   *msg,  /* buffer having the message data */
    int    msgSize  /* size of the message/data in bytes */
    )
{
  int i;


  /* Print the message to be sent byte by byte as character */
  printf("Reply sent: ");
  for(i=0; i<msgSize; i++)
  {
    printf("%c",msg[i]);
  }
  printf("\n");

  if((send(s, msg, msgSize, 0)) < 0) /* socket interface call to transmit */
  {
    perror("unable to send ");
    return(ER_SEND_FAILED);
  }

  return(OK); /* successful send */
}


/*
 * receiveMessage
 *
 * Function to receive message from client ftp
 *
 * Parameters
 * s    - Socket to be used to receive msg from client (input)
 * buffer   - Pointer to character arrary to store received msg (input/output)
 * bufferSize - Maximum size of the array, "buffer" in octent/byte (input)
 *        This is the maximum number of bytes that will be stored in buffer
 * msgSize  - Actual # of bytes received and stored in buffer in octet/byes (output)
 *
 * Return status
 *  OK      - Msg successfully received
 *  R_RECEIVE_FAILED  - Receiving msg failed
 */


int receiveMessage (
    int s,        /* socket */
    char *buffer,   /* buffer to store received msg */
    int bufferSize, /* how large the buffer is in octet */
    int *msgSize,   /* size of the received msg in octet */
    int p           /* print the message */
    )
{
  int i;

  *msgSize=recv(s,buffer,bufferSize,0); /* socket interface call to receive msg */

  if(*msgSize<0)
  {
    perror("unable to receive");
    return(ER_RECEIVE_FAILED);
  }

  /* Print the received msg byte by byte */
  if (p) {
    printf("received: ");
    for(i=0;i<*msgSize;i++)
    {
      printf("%c", buffer[i]);
    }
    printf("\n");
  }

  return (OK);
}



