/* rfsvcfn.c
 *
 * This is the server side code for the RPC (NFS) project.
 * it contains the functions used by the client such as
 * openfile, writefile, readfile, and closefile
 */

#include <stdio.h>
#include <rpc/rpc.h>

#include "rf.h"

#define OKAY 0
#define FAILED -1

FILE *filedesc[1]; /* can open up to one file at a time. */
long fd = 0; /* index into filedesc */


/* Default timeout can be changed using clnt_control() */
static struct timeval TIMEOUT = { 25,0 };


/*
 * rf_openfile_1_svc:
 * RPC procedure to open server file
 * NOTE: This file is local to the server and hence fopen is used.
 *
 *
 *  input parameters:
 *    openArg: 
 *      the open request struct with fields pertaining to opening a file
 *      filename: the filename of the server file to be opened
 *      mode: the mode of the file (readonly or writeonly)
 *
 *  output parameters:
 *    returns a pointer to a reply struct with the following fields:
 *      long openStatus -> 0: succes, else: fail
 *      long fd -> file descriptor
 */

RF_OpenFileReply_T *rf_openfile_1_svc(RF_OpenFileRequest_T *openArg,
    struct svc_req *rqstp)
{
  static RF_OpenFileReply_T res;

  printf("RF Server: Filename to open %s\n", openArg->filename);


  filedesc[fd] = fopen(openArg->filename, openArg->mode); 
  if (filedesc[fd] != NULL)
    res.openStatus = OKAY;
  else
    res.openStatus = FAILED;

  res.fd = fd;

  return(&res);
}

/*
 * rf_readfile_1_svc:
 * RPC procedure to read from the server file
 * NOTE: This file is local to the server and hence fread is used.
 *
 *
 *  input parameters:
 *    readArg: 
 *      read request struct containing fields for a read request
 *      see rf.x for further info on fields
 *
 *  returns a pointer to a reply struct with the following fields:
 *    long readStatus -> 0: success, else: fail
 *    bytesRead -> # of bytes read from file
 *    char buf[64] -> content read from file
 */


RF_ReadFileReply_T *rf_readfile_1_svc(RF_ReadFileRequest_T *readArg,
    struct svc_req *rqstp)
{
  static RF_ReadFileReply_T res;

  printf("RF Server: Recieved file read request.\n");

  res.bytesRead = fread(res.buf,
                        1,
                        readArg->bytesToRead,
                        filedesc[readArg->fd]);

  if (res.bytesRead >= 0)
    res.readStatus = OKAY;
  else
    res.readStatus = FAILED;

  return(&res);
}

/*
 * rf_writefile_1_svc:
 * RPC procedure to write to server file
 * NOTE: This file is local to the server and hence fwrite is used.
 *
 *
 *  input parameters:
 *    writeArg:
 *      write request struct containing the following fields:
 *        fd: file descriptor
 *        bytesWrite: bytes to write to the file
 *        buf: an array containing the data to be written
 *
 *  returns a pointer to a reply struct with the following fields:
 *    long bytesWritten -> indicates the number of bytes written
 */
RF_WriteFileReply_T *rf_writefile_1_svc(RF_WriteFileRequest_T *writeArg,
    struct svc_req *rqstp)
{
  static RF_WriteFileReply_T res;

  printf("RF Server: Received file write request.\n");

  res.bytesWritten = fwrite(writeArg->buf,
                            1,
                            writeArg->bytesWrite,
                            filedesc[writeArg->fd]);
  return(&res);
}

/*
 * rf_closefile_1_svc:
 * RPC procedure to close server file
 * NOTE: This file is local to the server and hence fclose is used.
 *
 * This function takes a RF_closeFileRequest_T pointer containing a
 * file descriptor and closes the file
 *
 * returns a reply containing the status of the close operation
 */

RF_CloseFileReply_T *rf_closefile_1_svc(RF_CloseFileRequest_T *closeArg,
    struct svc_req *rqstp)
{
  static RF_CloseFileReply_T res;

  printf("RF Server: Recieved close file request.\n");

  res.closeStatus = fclose(filedesc[closeArg->fd]);

  return(&res);
}
