/* rftest.c
// Client main program to test RF server.
//
// NOTE: This code is not compiled and hence may have syntax error as well as
// logic error. You have to fix them, if they exist
//
// Run this program with UNIX server IP address as the argument like
//       rfclient  192.168.200.230
//
// Complete the rest of the documentation here for the program
//
*/

#include <stdio.h>
#include <rpc/rpc.h>
#include <string.h>

#include "rf.h"

#define RF_PROGRAM 20
#define RF_VERSION 1

#define OKAY 0
#define FAILED -1

int main (int argc, char *argv[]) {
  CLIENT *rf_clnt;
  char *server;
  long status;
  long fd;
  char line_stdin[2048];
  char remote_file[1024] = {"remote_nfs_file.txt"};

  RF_OpenFileRequest_T openReq;
  RF_OpenFileReply_T *openReply;
  RF_ReadFileRequest_T readReq;
  RF_ReadFileReply_T *readReply;
  RF_CloseFileRequest_T closeReq;
  RF_CloseFileReply_T *closeReply;

  RF_WriteFileRequest_T writeReq;
  RF_WriteFileReply_T *writeReply;

  if (argc != 2) {
    printf("Usage: %s server-IP Address\n", argv[0]);
    exit(-1);
  }

  server = argv[1]; /* get server name. */

  // Create client handle.
  //

  printf("Calling RF clnt_create()\n");
  if ((rf_clnt = clnt_create(server, RF_PROGRAM, RF_VERSION, "udp")) == NULL) {
    clnt_pcreateerror(server);
    exit(-2);
  }


  //
  // For the project modify code below to do two things:
  // (1) to copy a file from the server and store it locally on the client
  // (your system)
  // (2) to copy a local file from your system and store it on the server
  // Read ONLY 64 bytes at a time
  // Test your program with two files of size 200 to 300 bytes/character.
  //
  // To produce output, you must print the content of file sent from client
  // after read as well as content of file received through rpc procedure call.
  //


  // (1) Copy remote server file to local client system
  printf("Copy remote server file to local client file\n");

  // Read (1) client filename and (2) server filename here
  printf("client filename: ");
  char clientFilename[81];
  gets(clientFilename);

  printf("server filename: ");
  char serverFilename[81];
  gets(serverFilename);

  // Open local client file here - add this code
  FILE *clientFile = fopen(clientFilename, "w");

  // call open the remote server file using rpc open file procedure
  printf("Trying rf_openfile_1()\n");

  strcpy(openReq.filename, serverFilename);
  strcpy(openReq.mode, "r");  // set mode for reading the server file

  openReply = rf_openfile_1(&openReq, rf_clnt);
  if (openReply == NULL) {
    printf("rf_openfile_1 failed.\n");
    clnt_perror(rf_clnt, server);
    exit(-1);
  } else {
    status = openReply->openStatus;
    if (status == OKAY) {
      fd = openReply->fd;
      printf("RF open is sucessful. FD: %ld\n", fd);
    } else {
      printf("ERROR RF open status = %ld\n", status);
      exit(-1);
    }
  }


  // Read from remote server file 64 bytes at a time until end of file or error

  printf("Trying RF read file.\n");

  readReq.fd = fd;
  readReq.bytesToRead = 64;

  // There are three ways to exit this loop. 
  // (1) readReply is NULL, which means the rpc procedure call failed due to
  // network error or rpc server not running
  // (2) rpc call is successful, however, read operation failed with bytesread
  // is negative
  // (3) end of file has reached, bytesread is zero
  // This loop could use some refining but it is functional.

  while ((readReply = rf_readfile_1(&readReq, rf_clnt)) != NULL) 
  {
    printf("rpc call successful. Checking rpc return status from server\n");
    status = readReply->readStatus;
    if (status == OKAY) 
    {
      printf("Successful read from server. Checking for end of file or not\n");
      if (readReply->bytesRead > 0) {
        printf("RF read file successful with bytesRead = %ld.\n",
               readReply->bytesRead);

        // Print what is read.
        // Following printf assumes NULL terminated string, which is invalid
        // You must change this to print one character at a time using for
        // loop like in ftp
        for (int i = 0; i < readReply->bytesRead; i++) {
          printf("%c", (char)readReply->buf[i]);
        }
        printf("\n");

        // Write to local client file here - add the code
        fwrite(readReply->buf, 1, readReply->bytesRead, clientFile);

      }
      else 
      {
        printf("ERROR Bytes read is zero or negative, bytesRead = %ld\n",
               readReply->bytesRead);
        break;
      }
    } 
    else 
    {
      printf("ERROR rpc server procedure returned error. RF status = %ld\n",
             status);
      break;
    }
  }

  // This is the error handling for the readReply is NULL. 
  if (readReply == NULL) {
    printf("ERROR RPC: RF procedure call failed due to network error");
    printf("or rpc server not running\n");
    clnt_perror(rf_clnt,server);
  }


  // Close local client file - add the code here
  fclose(clientFile);

  // close the server file
  printf("Calling RF close file.\n");

  closeReq.fd = fd;
  closeReply = rf_closefile_1(&closeReq, rf_clnt);

  if (closeReply == NULL) 
  {
    printf("RF close file failed.\n");
    clnt_perror(rf_clnt, server);
  } 
  else 
  {
    status = closeReply->closeStatus;
    if (status == OKAY) 
    {
      printf("RF close file successful.\n");
      fd = (int)NULL;
    } 
    else 
    {
      printf("ERROR RF close status = %ld\n", status);
    }
  }


  // Copy local client file to the server here
  // The following are the steps (pseudo code)
  printf("Copy file from client to server\n");

  // Read two filenames: client filename and server filename
  printf("client filename: ");
  gets(clientFilename);

  printf("server filename: ");
  gets(serverFilename);


  // open local client file
  clientFile = fopen(clientFilename, "r");

  printf("Trying rf_openfile_1()\n");

  // open server file
  strcpy(openReq.filename, serverFilename);
  strcpy(openReq.mode, "w");

  openReply = rf_openfile_1(&openReq, rf_clnt);
  if (openReply == NULL) {
    printf("rf_openfile_1 failed.\n");
    clnt_perror(rf_clnt, server);
    exit(-1);
  } else {
    status = openReply->openStatus;
    if (status == OKAY) {
      fd = openReply->fd;
      printf("RF open is successful. FD: %ld\n", fd);
    } else {
      printf("ERROR RF open status = %ld\n", status);
      exit(-1);
    }
  }

  //   while (not end of file on local file)
  //   {
  //      read from local file 64 bytes size
  //      print the content read
  //      write to server file using rf_writefile_1 procedure
  //   }
  char buffer[64];
  while (!feof(clientFile)) {
    size_t read = fread(buffer, 1, sizeof(buffer), clientFile);
    
    int i;
    for (i = 0; i < read; i++) {
      printf("%c", (char)buffer[i]);
    }
    printf("\n");

    writeReq.fd = openReply->fd;
    writeReq.bytesWrite = read;
    for (i = 0; i < sizeof(buffer); i++) {
      writeReq.buf[i] = buffer[i];
    }

    writeReply = rf_writefile_1(&writeReq, rf_clnt);
    if (writeReply == NULL) {
      printf("rf_writefile_1 failed.\n");
      clnt_perror(rf_clnt, server);
      exit(-1);
    } else {
      if (writeReply->bytesWritten == writeReq.bytesWrite) {
        printf("RF write successful\n");
      } else {
        printf("ERROR RF bytes written = %ld\n", status);
      }
    }
  }

  // Close server file
  closeReq.fd = openReply->fd;
  closeReply = rf_closefile_1(&closeReq, rf_clnt);

  if (closeReply == NULL) {
    printf("RF close file failed.\n");
    clnt_perror(rf_clnt, server);
  } else {
    status = closeReply->closeStatus;
    if (status == OKAY) {
      printf("RF close file successful.\n");
      openReply->fd = (int)NULL;
    } else {
      printf("ERROR RF close status = %ld\n", status);
    }
  }

  // close local file
  fclose(clientFile);

  // Close rpc transport connection with the server
  printf("Calling RF close file.\n");

  clnt_destroy(rf_clnt);

  // Exit main program.
  printf("Exiting main test program.\n\n");

  exit(0);
}

